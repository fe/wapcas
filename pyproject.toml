# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

[tool.bandit]
targets = ["src/"]
exclude_dirs = [
  "src/wapcas/test_annotations_router.py",
  "src/wapcas/test_collections_router.py",
  "src/wapcas/test_extensions_router.py",
]

[tool.black]
line-length = 88
target-version = ["py312"]


[tool.commitizen]
name = "cz_conventional_commits"
version = "0.1.0"
tag_format = "$version"


[tool.coverage.html]
directory = "docs/coverage"

[tool.coverage.report]
# Regexes for lines to exclude from consideration
exclude_also = [
  # Don't complain if non-runnable code isn't run:
  "if __name__ == .__main__.:",
]
fail_under = 85

[tool.coverage.run]
branch = true
omit = [
  # omit testfiles
  "*/test_*",
]
source = ["src/"]

[tool.coverage.xml]
output = "docs/coverage/coverage.xml"


[tool.interrogate]
ignore-init-method = true
ignore-init-module = true
fail-under = 95
# ignore fixture/test functions the self explaining head and options handlers
ignore-regex = ["^fixture_.*", "^test_.*", "^head_.*", "^options_.*"]
ext = []
# possible values: 0 (minimal output), 1 (-v), 2 (-vv)
verbose = 2
quiet = false
color = true
omit-covered-files = false
output = "docs/coverage/interrogate.txt"
# output file logation
generate-badge = "./docs/img/interrogate.svg"
badge-format = "svg"


[tool.isort]
atomic = true
combine_as_imports = true
line_length = 88
profile = "black"
py_version = 312
skip_gitignore = true

[tool.mypy]
# Specify the target platform details in config, so your developers are
# free to run mypy on Windows, Linux, or macOS and get consistent
# results.
python_version = "3.12"
mypy_path = "src"
exclude = ["^test_"]
# strict = true

[tool.pydocstringformatter]
write = true
max-line-length = 88
linewrap-full-docstring = true
style = "pep257"
split-summary-body = false

[tool.pylint.main]
extension-pkg-allow-list = ["pydantic"]
fail-under = 9.0
ignore-paths = ["src/wapcas/test_"]
py-version = "3.12"

[tool.pylint.basic]
good-names = ["i", "j", "k", "l", "m", "n", "_"]

[tool.pylint.format]
max-line-length = 88

[tool.pylint."messages control"]
disable = [
  "invalid-name",
  "raw-checker-failed",
  "bad-inline-option",
  "locally-disabled",
  "file-ignored",
  "suppressed-message",
  "useless-suppression",
  "deprecated-pragma",
  "use-implicit-booleaness-not-comparison-to-string",
  "use-implicit-booleaness-not-comparison-to-zero",
  "use-symbolic-message-instead",
]

[tool.pylint.reports]
output-format = "json2:docs/coverage/pylint.json,colorized"

[tool.semantic_release]
commit_message = "chore(release): {version}"
major_on_zero = false
no_git_verify = true
tag_format = "{version}"
version_toml = ["pyproject.toml:tool.somesy.project.version"]
version_variables = ["src/wapcas/__init__.py:__version__"]

[tool.semantic_release.changelog]
exclude_commit_patterns = [
  "^Merge",
  "^Revert",
  "^test",
  "^refactor",
  "^docs",
  "^ci",
  "^chore",
  "^build",
]

[tool.semantic_release.remote]
domain = "gitlab.gwdg.de"
token = { env = "GL_TOKEN" }
type = "gitlab"

[tool.somesy.project]
name = "WAPCAS"
version = "0.3.0"
description = "An implementation of the Web Annotation Protocol backed by a MongoDB as storage."

keywords = [
  "web annotations",
  "annotations",
  "wap",
  "web annotation data model",
  "web annotation protocol",
  "annotation store",
]
license = "EUPL-1.2"
repository = "https://gitlab.gwdg.de/fe/wapcas"

[[tool.somesy.project.people]]
given-names = "Stefan"
family-names = "Hynek"
email = "stefan.hynek@uni-goettingen.de"
orcid = "https://orcid.org/0000-0002-3485-0746"
affiliation = "https://ror.org/05745n787"
author = true
maintainer = true

[tool.somesy.config]
verbose = true
