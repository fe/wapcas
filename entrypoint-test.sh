#!/bin/bash
# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

set -euxo pipefail
python src/dbinit.py
coverage run -m pytest --cache-clear --junitxml=docs/coverage/junit.xml
coverage html
coverage xml
genbadge coverage --input-file=docs/coverage/coverage.xml --output-file=docs/img/coverage.svg
genbadge tests --input-file=docs/coverage/junit.xml --output-file=docs/img/tests.svg
interrogate src/
coverage report
bandit --configfile pyproject.toml --format html --output docs/coverage/security.html --recursive src/
pylint --persistent n src/
anybadge --label pylint --value `jq ".statistics.score" docs/coverage/pylint.json` --overwrite --file docs/img/pylint.svg 2=red 4=orange 8=yellow 10=green
