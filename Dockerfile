# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

FROM docker.io/python:3.12-slim

# Static labels
LABEL \
    org.opencontainers.image.licenses="EUPL-1.2" \
    org.opencontainers.image.authors="Stefan Hynek" \
    org.opencontainers.image.title="Web Annotation Protocol Compliant Annotation Store" \
    org.opencontainers.image.description="An implementation of the Web Annotation Protocol." \
    org.opencontainers.image.url="https://gitlab.gwdg.de/fe/wapcas" \
    org.opencontainers.image.source="https://gitlab.gwdg.de/fe/wapcas" \
    org.opencontainers.image.vendor="SUB/FE"

# Install requirements
WORKDIR /
COPY ./requirements.txt /requirements.txt
# hadolint ignore=DL3013
RUN pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir -r /requirements.txt

WORKDIR /app
COPY src /app

# Dynamic labels
ARG IMG_DATE
ARG IMG_REVISION
ARG IMG_VERSION
LABEL \
    org.opencontainers.image.created="${IMG_DATE}" \
    org.opencontainers.image.revision="${IMG_REVISION}" \
    org.opencontainers.image.version="${IMG_VERSION}"

# Uvicorn configuration environment variables's default values
ENV UVICORN_HOST=0.0.0.0
ENV UVICORN_PORT=80
ENV UVICORN_LOG_LEVEL=info
CMD ["uvicorn", "main:app", "--proxy-headers"]

COPY Dockerfile /
