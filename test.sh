# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

export USER=$(id -u)
export GROUP=$(id -g)
docker compose -f docker-compose.test.yml up \
  --build \
  --always-recreate-deps \
  --force-recreate \
  --exit-code-from test \
  --attach test
