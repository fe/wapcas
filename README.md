<!--
SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen

SPDX-License-Identifier: CC0-1.0
-->

# WAPCAS (Web Annotation Protocol Compliant Annotation Store)

[![REUSE status](https://api.reuse.software/badge/gitlab.gwdg.de/fe/wapcas)](https://api.reuse.software/info/gitlab.gwdg.de/fe/wapcas)
[![Coverage](https://fe.pages.gwdg.de/wapcas/img/coverage.svg)](https://fe.pages.gwdg.de/wapcas/coverage/index.html)
[![Interrogate](https://fe.pages.gwdg.de/wapcas/img/interrogate.svg)](https://fe.pages.gwdg.de/wapcas/coverage/interrogate.txt)

An implementation of the [Web Annotation Protocol](https://www.w3.org/TR/annotation-protocol/) backed by a MongoDB as storage.

## Development

### Requirements

- Python >= 3.12
- Docker >=20.10, Docker Compose >= 2

Set up virtual environment and dev tools

```sh
python -m venv venv
. venv/bin/activate
pip install -r requirements.txt -r requirements.dev.txt
pre-commit install
```

### Build and Run

```sh
docker compose up --build
```

The API is served from `http://localhost:8000/`, Swagger UI is available at [http://localhost:8000/docs](http://localhost:8000/docs), MongoExpress is available at [http://localhost:8081](http://localhost:8081).

### Testing

Tests can be run independently from the dev stack with a helper script

```sh
./test.sh
```

After the tests have run, you could inspect the code coverage by opening `docs/coverage/index.html` in a browser.

## Configuration

The application reads the following variables from the environment:

| Name | Description | Default |
| -----|-------------|---------|
| MONGODB_URL | The URL to connect to MongoDB. | `mongodb://root:example@mongo:27017/` |
| MONGODB_DATABASE | The MongoDB database name. | `wapcas` |
| SENTRY_DSN | [Sentry Data Source Name](https://docs.sentry.io/platforms/python/configuration/options/#dsn) for the Service. If unset, disables Sentry Error Tracking. | `None` |
| SENTRY_ENVIRONMENT | The [Sentry Environment](https://docs.sentry.io/platforms/python/configuration/environments/) name. | `production`|
| SENTRY_RELEASE | The [Sentry Release](https://docs.sentry.io/platforms/python/configuration/releases/) version of the application. | `None` |
| UVICORN_HOST | The [Uvicorn host](https://www.uvicorn.org/settings/#socket-binding) name. | `0.0.0.0` |
| UVICORN_LOG_LEVEL | The [Uvicorn log level](https://www.uvicorn.org/settings/#logging). | `info` |
| UVICORN_PORT | The [Uvicorn service port](https://www.uvicorn.org/settings/#socket-binding). | `80` |
| WAPCAS_BASE_URL | The URL that is used as base for Object IDs. | `http://localhost` |
| WAPCAS_DEFAULT_PAGE_SIZE | The default collection page size. | `10` |
| WAPCAS_LOG_LEVEL | The basic log level. | `INFO` |

## Roadmap

### Phase I (POC) 2023-01-23–2023-04-12

- no release
- CREATE/RETRIEVE Annotations, e.g. GET `/<container_id>/<annotation_id>`, POST `/<container_id>`
- CREATE/RETRIEVE Annotation Containers, e.g. GET `/<container_id>/`, POST `/`

### Phase II (Alpha) 2024-01-22–2024-04-22

- pre-release (pre 0.1.0)
- backend switch to MongoDB
- Annotation Pages, e.g. GET /<container_id>/?page=0

### Phase III (Beta) 2024-04-15–2024-07-31

- first release (pre 1.0.0)
- prepare API extensions (`/_/`)
- prepare k8s deployment
  - split db preparation tasks into a script for an init-container
  - provide service health endpoint

### Phase IV (Stable)

- 1.0.0 release

## Architecture

![WAPCAS Architecture](docs/img/architecture.svg)

## License

This project aims to be [REUSE compliant](https://api.reuse.software/info/gitlab.gwdg.de/fe/wapcas).
Original parts are licensed under EUPL-1.2.
Derivative code is licensed under the respective license of the original.
Documentation, configuration and generated code files are licensed under CC0-1.0.
