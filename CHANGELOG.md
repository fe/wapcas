# CHANGELOG


## v0.3.0 (2025-03-05)

### Bug Fixes

- **annotations**: Ensure etag consistency by applying DEFAULT_MODEL_DUMP_OPTIONS during
  serialization
  ([`e44ebac`](https://gitlab.gwdg.de/fe/wapcas/-/commit/e44ebac7295d6629727ea78e14f1ea0bb7309151))

- **constants**: Change type of some String constants that are actually Literals
  ([`facd0e2`](https://gitlab.gwdg.de/fe/wapcas/-/commit/facd0e29203b51277d8438c7e0f47f0870f4866f))

### Code Style

- **exceptions**: Add trailing full stop to class docs
  ([`ba362b7`](https://gitlab.gwdg.de/fe/wapcas/-/commit/ba362b7c7c70bdf4932649a2e960a6204a330ae7))

### Features

- **models**: Implement all body and target models
  ([`224ffa7`](https://gitlab.gwdg.de/fe/wapcas/-/commit/224ffa7d73392eef91182a598552c33ddaafa5b2))

also, merge EmptyCollection with Collection

- **serializers**: Add a pydantic serializer for potentially multiple context field values
  ([`a43a86c`](https://gitlab.gwdg.de/fe/wapcas/-/commit/a43a86c4c53ed0dd49b84d39d472f29875cf3320))


## v0.2.1 (2024-12-13)

### Bug Fixes

- **collections**: Return a page if it exists
  ([`3fa1aae`](https://gitlab.gwdg.de/fe/wapcas/-/commit/3fa1aae61b64e1febfa5201212d7dd400f4a5cf0))

the missing return statement led to the collection always be returned

close #26


## v0.2.0 (2024-11-22)

### Bug Fixes

- Add constant for reserved characters; add corresponding exception
  ([`b54dac9`](https://gitlab.gwdg.de/fe/wapcas/-/commit/b54dac99e3eea1c199908360deb5e2b30b398467))

- **constants**: Add more possibly confusing characters to disallow in identifiers
  ([`2d4b6d8`](https://gitlab.gwdg.de/fe/wapcas/-/commit/2d4b6d8b9a1b061ff67f501db57eef502ac1fc96))

### Features

- Check for reserved characters on annotation and collection creation
  ([`321a784`](https://gitlab.gwdg.de/fe/wapcas/-/commit/321a784352861cf65740af5681e2c3d8a8b1718e))

this is meant to prevent URL confusion attacks


## v0.1.0 (2024-07-09)

### Bug Fixes

- Handle reserved collection names on retrieve
  ([`a095683`](https://gitlab.gwdg.de/fe/wapcas/-/commit/a09568366fb3e584125d20e98bc9e5d322217a53))

- **annotations**: Move find_one_and_replace into the try block of the transaction
  ([`0195e41`](https://gitlab.gwdg.de/fe/wapcas/-/commit/0195e417b206a67b99e17923cb8fe4f8fb3ef7d4))

- **annotations_router**: Add missing handlings of RESERVED_COLLECTION_NAMES
  ([`1555b85`](https://gitlab.gwdg.de/fe/wapcas/-/commit/1555b85e46e9e7b003e8c68e3ad9d6fd53548019))

- **constants**: Add string literals for "context" and "type" field names and (default) values
  ([`8b8bca2`](https://gitlab.gwdg.de/fe/wapcas/-/commit/8b8bca24ee768b704071f3115e638a75a21129c9))

- **Dockerfile**: Add missing space
  ([`83b984e`](https://gitlab.gwdg.de/fe/wapcas/-/commit/83b984e91b7a8ee6f448740b99ccfbdafe35562a))

- **Dockerfile**: Ignore pip upgrade fixed version
  ([`6ea59c7`](https://gitlab.gwdg.de/fe/wapcas/-/commit/6ea59c765559d8cd46f2f13dcb0afda3ebc06929))

- **main**: Apply fixed formatting; use native dict type instead of Dict; test newly implemented
  ErrorMessage model on create_collection's 409 response
  ([`fa934ec`](https://gitlab.gwdg.de/fe/wapcas/-/commit/fa934ec8b6defbcd50e07012bd83f850bff07aae))

- **main**: Set response headers on retrieve_collection before handling of pages
  ([`5aee1de`](https://gitlab.gwdg.de/fe/wapcas/-/commit/5aee1deffcc8720c71b329e124545fd6124731e5))

- **models**: Remove field alias "@id" for "id"
  ([`499ebf0`](https://gitlab.gwdg.de/fe/wapcas/-/commit/499ebf00b04fcba5edd8a3297d584a3da769e4f2))

this was erroneously introduced

- **models**: Set default for Collection.total=0; improve Page model
  ([`e905629`](https://gitlab.gwdg.de/fe/wapcas/-/commit/e90562927ceb1162c7a7238739ea4907baab79fa))

- **models**: Tighten model field constraints and validation
  ([`971a6a0`](https://gitlab.gwdg.de/fe/wapcas/-/commit/971a6a08d8129443757fc19bd16364fafb68bfb9))

enforce basic compliance with WAP and WA datamodel with pydantic models and validation

- **models**: Update models to pydantic v2 syntax; allow aliased fields be populated by field name
  ([`58060bc`](https://gitlab.gwdg.de/fe/wapcas/-/commit/58060bcf025bbeba7809de3f6b17aa7f2d1c632c))

### Features

- Implement complete CRUD for Annotations and AnnotationCollections
  ([`f35730f`](https://gitlab.gwdg.de/fe/wapcas/-/commit/f35730ff6ee297c1864800d35639b3af76c13dce))

as pages are a requirement for container representations, this is currently implemented as a stub;
  only one page with index=0 is created with unlimited size

- **annotations**: Update a collection’s "modified" field on replacement of an annotation
  ([`4ad12bb`](https://gitlab.gwdg.de/fe/wapcas/-/commit/4ad12bb3d8c3deeb4c83d410d35971ab9f395b62))

also, improve return type coverage, documentation consistency, and be even more rigid about
  validation

- **create_annotation**: Use transactions for multi-collection writes
  ([`3ea2cf7`](https://gitlab.gwdg.de/fe/wapcas/-/commit/3ea2cf7a4ee61c73af7543877881ea917ad2854f))

this allows for rolling back changes if one of the operations in the transactions fail

BREAKING CHANGE: requires a mongodb replica set deployment

- **dbinit**: Move database client and initialization to own module
  ([`7ab5797`](https://gitlab.gwdg.de/fe/wapcas/-/commit/7ab5797d3e50291d74ff282ed312332d0b87c237))

- **exceptions**: Implement DatabaseTransactionAbort for transactions that have gone wrong
  ([`1549d3d`](https://gitlab.gwdg.de/fe/wapcas/-/commit/1549d3d8413e459e275b08887ee1e1531e0bc4d1))

- **logging**: Implement HealthCheckFilter to let uvicorn logger filter health check logs\n\nalso,
  factor out constant string literals into module "constants"
  ([`59fc937`](https://gitlab.gwdg.de/fe/wapcas/-/commit/59fc937f95a1e02255b9a273143c074b95d9d2ad))

- **main**: Implement health check endpoint\n\nalso, remove constants now in own module; remove all
  db initialization statements; check for reserved collection names where applicable; add some more
  documentation on response types; make prefer-header evaluation more tolerant; refactor request
  options to be DRYer; refactor update_or_create_page to be more self-explaining; add return type
  hints to all exception class inits
  ([`6ef49a8`](https://gitlab.gwdg.de/fe/wapcas/-/commit/6ef49a8f6476b15c7a8e177b29a65516a7f54786))

- **main**: Integrate Sentry SDK
  ([`27195e8`](https://gitlab.gwdg.de/fe/wapcas/-/commit/27195e86594a32406dbae9868855c8d5454466fa))

the sentry sdk is loaded in a fail-safe manner so that the startup of the service is by no means
  interrupted

close #5

- **main**: Provide basic web annotation API service
  ([`958cc59`](https://gitlab.gwdg.de/fe/wapcas/-/commit/958cc5939b2214a959fb51e5f938a8a3e8c91cc9))

this feature implements create and retrieve for annotation collections and annotations as a
  proof-of-concept with FastAPI

- **models**: Add an ErrorMessage model for HTTP error responses
  ([`ea84554`](https://gitlab.gwdg.de/fe/wapcas/-/commit/ea84554c0e053e3d81651671d2411d90d70b176b))

- **models**: Add more specific page and collection models for stricter validation
  ([`72ba6a9`](https://gitlab.gwdg.de/fe/wapcas/-/commit/72ba6a9fe4bdf2de4076f844b3be3c60e498a7b4))

- **models**: Implement InputAnnotation that allows extra properties\n\nalso, move models.py to
  wapcas package
  ([`54e3112`](https://gitlab.gwdg.de/fe/wapcas/-/commit/54e31122fce094ac84dd6fdd1a5710eb4813cb4d))

- **models**: Provide classes for Annotations, Collections and Pages
  ([`fbee692`](https://gitlab.gwdg.de/fe/wapcas/-/commit/fbee692f17ee88a2f1ca8a5276b87bfdb82b4c17))

the models will be used for input and output validation by FastAPI

- **pages**: Implement full featured collection pages resource
  ([`6300720`](https://gitlab.gwdg.de/fe/wapcas/-/commit/6300720f9825c7439dc0e998ec7883d8af61c399))

also, make mongodb connection and database, log level, default page size and base url configurable
  from env; use json instead of pickle for generating etag for security reasons; create indexes for
  key "id" in all db collections

- **validators**: Implement a validator that checks for a certain item in a list
  ([`051d83d`](https://gitlab.gwdg.de/fe/wapcas/-/commit/051d83da059b049f66422ab82be34d70a251cb68))

this is needed for the models to stay flexible concerning additional contexts and types but to
  preserve the ability to validate the minimum requirements

close #20
