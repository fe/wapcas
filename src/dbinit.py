# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: EUPL-1.2

"""Script for initialization of the database."""

import contextlib
import logging
import os
from typing import Any, Final

from pymongo import MongoClient
from pymongo.errors import CollectionInvalid

import wapcas.logging
from wapcas.constants import (
    ANNOTATIONS_NAME,
    COLLECTIONS_NAME,
    COLLECTIONS_WITH_EMBEDDED_ANNOTATIONS_NAME,
    COLLECTIONS_WITH_EMBEDDED_PAGES_NAME,
    PAGES_NAME,
    PAGES_WITH_EMBEDDED_ANNOTATIONS_NAME,
)

# Set up the database client. Defaults to a single node connection string.
MONGODB_URL: Final[str] = os.getenv(
    "MONGODB_URL", "mongodb://root:example@mongo:27017/"
)
mongo_client: Final[MongoClient[dict[str, Any]]] = MongoClient(MONGODB_URL)

# Set up the database.
MONGODB_DATABASE: Final[str] = os.getenv("MONGODB_DATABASE", "wapcas")
db: Final = mongo_client[MONGODB_DATABASE]

# Initialize collections and indexes.
if __name__ == "__main__":
    wapcas.logging.init()
    logger = logging.getLogger(__name__)
    logger.info("Begin database initialization.")
    # Create collections but suppress the CollectionInvalid exception that is raised
    # when collections already exist. Also, we will not specify a timeout for the
    # operations because the default (20s) is fine.
    with contextlib.suppress(CollectionInvalid):
        db.create_collection(COLLECTIONS_NAME)
        db.create_collection(PAGES_NAME)
        db.create_collection(ANNOTATIONS_NAME)

    # Create indices in all collections on field/key "id"
    db[COLLECTIONS_NAME].create_index("id", unique=True)
    db[PAGES_NAME].create_index("id", unique=True)
    db[ANNOTATIONS_NAME].create_index("id", unique=True)

    with contextlib.suppress(CollectionInvalid):
        db.create_collection(
            # This is the view for Collections with "PREFER_IRIS"
            COLLECTIONS_WITH_EMBEDDED_PAGES_NAME,
            viewOn=COLLECTIONS_NAME,
            pipeline=[
                {
                    "$lookup": {
                        "from": PAGES_NAME,
                        "localField": "first",
                        "foreignField": "id",
                        "as": "first",
                    }
                },
                # destruct array
                {"$unwind": {"path": "$first"}},
                # remove _id, @context, partOf
                {
                    "$project": {
                        "_id": False,
                        "first._id": False,
                        "first.@context": False,
                        "first.partOf": False,
                    }
                },
            ],
        )

    with contextlib.suppress(CollectionInvalid):
        db.create_collection(
            # This is the view for Collections with "PREFER_DESCRIPTIONS"
            COLLECTIONS_WITH_EMBEDDED_ANNOTATIONS_NAME,
            viewOn=COLLECTIONS_WITH_EMBEDDED_PAGES_NAME,
            pipeline=[
                {
                    "$lookup": {
                        "from": ANNOTATIONS_NAME,
                        "localField": "first.items",
                        "foreignField": "id",
                        "as": "first.items",
                    }
                },
                # remove _id, @context, partOf
                {
                    "$project": {
                        "first.items._id": False,
                        "first.items.@context": False,
                    }
                },
            ],
        )

    with contextlib.suppress(CollectionInvalid):
        db.create_collection(
            # This is the view for Pages with "PREFER_DESCRIPTIONS"
            PAGES_WITH_EMBEDDED_ANNOTATIONS_NAME,
            viewOn=PAGES_NAME,
            pipeline=[
                {
                    "$lookup": {
                        "from": ANNOTATIONS_NAME,
                        "localField": "items",
                        "foreignField": "id",
                        "as": "items",
                    }
                },
                # remove _id, @context
                {
                    "$project": {
                        "items._id": False,
                        "items.@context": False,
                    }
                },
            ],
        )

    logger.info("Finished database initialization. Success!")
