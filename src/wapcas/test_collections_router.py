# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the Collections API router."""

import uuid

import pytest
from fastapi.testclient import TestClient

from wapcas.annotations_router import router as arouter
from wapcas.collections_router import router
from wapcas.constants import (
    CONTAINER_PREFER_DESCRIPTIONS,
    CONTAINER_PREFER_IRIS,
    CONTAINER_PREFER_MINIMAL,
    OA_COLLECTION_CONTEXT_FIELD_DEFAULT_VALUE,
    RESERVED_COLLECTION_NAMES,
)
from wapcas.exceptions import (
    CollectionExists,
    CollectionMismatch,
    CollectionNameReserved,
    CollectionNotEmpty,
    CollectionNotFound,
    ReservedCharacterUsedInIdentifier,
)
from wapcas.utils import BASE_URL

client = TestClient(router)
"""A Collection Router Test Client."""
aclient = TestClient(arouter)
"""An Annotation Router Test Client."""


def test_root_options():
    response = client.options("/")
    assert response.status_code == 204


def test_create():
    collection_slug = "test"
    response = client.post("/", headers={"slug": collection_slug})
    assert response.status_code == 201
    json = response.json()
    assert json["@context"] == OA_COLLECTION_CONTEXT_FIELD_DEFAULT_VALUE
    assert json["id"] == f"{BASE_URL}/{collection_slug}/"


def test_create_no_slug():
    response = client.post("/")
    assert response.status_code == 201
    json = response.json()
    assert json["@context"] == OA_COLLECTION_CONTEXT_FIELD_DEFAULT_VALUE
    assert json["id"].startswith(f"{BASE_URL}/")
    # generated id must end in a valid UUID, raises if invalid
    uuid.UUID(json["id"].split("/")[-2], version=4)


def test_create_query():
    collection_slug = "query?"
    with pytest.raises(ReservedCharacterUsedInIdentifier):
        client.post("/", headers={"slug": collection_slug})


def test_create_fragment():
    collection_slug = "fragment#"
    with pytest.raises(ReservedCharacterUsedInIdentifier):
        client.post("/", headers={"slug": collection_slug})


def test_create_with_slash():
    collection_slug = "slash"
    response = client.post("/", headers={"slug": collection_slug})
    assert response.status_code == 201
    json = response.json()
    # and the path component MUST end in a "/" character.
    assert json["id"].endswith("/")


def test_create_with_slash_beginning():
    collection_slug = "/slashbeginning"
    with pytest.raises(ReservedCharacterUsedInIdentifier):
        client.post("/", headers={"slug": collection_slug})


def test_create_with_slash_middle():
    collection_slug = "slash/middle"
    with pytest.raises(ReservedCharacterUsedInIdentifier):
        client.post("/", headers={"slug": collection_slug})


def test_create_with_slash_end():
    collection_slug = "slashend/"
    with pytest.raises(ReservedCharacterUsedInIdentifier):
        client.post("/", headers={"slug": collection_slug})


def test_create_existing():
    collection_slug = "existing"
    client.post("/", headers={"slug": collection_slug})
    with pytest.raises(CollectionExists):
        client.post("/", headers={"slug": collection_slug})


def test_create_reserved():
    with pytest.raises(CollectionNameReserved):
        for collection_slug in RESERVED_COLLECTION_NAMES:
            client.post("/", headers={"slug": collection_slug})


def test_retrieve_reserved():
    with pytest.raises(CollectionNameReserved):
        for collection_slug in RESERVED_COLLECTION_NAMES:
            client.get(f"/{collection_slug}")


def test_retrieve_not_exists():
    collection_slug = "notexisting"
    with pytest.raises(CollectionNotFound):
        client.get(f"/{collection_slug}")


def test_retrieve_empty_collection():
    collection_slug = "emptycollection"
    client.post("/", headers={"slug": collection_slug})
    response = client.get(f"/{collection_slug}", headers={"prefer": ""})
    assert response.status_code == 200
    json = response.json()
    keys = json.keys()
    assert "@context" in keys
    assert "id" in keys
    assert "type" in keys


def test_delete_reserved():
    with pytest.raises(CollectionNameReserved):
        for collection_slug in RESERVED_COLLECTION_NAMES:
            client.delete(f"/{collection_slug}")


def test_delete_not_exists():
    collection_slug = "notexisting"
    with pytest.raises(CollectionNotFound):
        client.delete(f"/{collection_slug}")


def test_delete_no_etag():
    slug = "existing"
    with pytest.raises(CollectionMismatch):
        client.delete(f"/{slug}")


def test_delete_no_match_etag():
    slug = "existing"
    with pytest.raises(CollectionMismatch):
        client.delete(
            f"/{slug}",
            headers={
                "if-match": "eb7c9b1a0392212353e8b110e7179f7705a9f4a2f8f80fc90a5c84c288b20a78"
            },
        )


def test_delete_not_empty():
    slug = "existing"
    data = {
        "@context": "http://www.w3.org/ns/anno.jsonld",
        "body": "http://example.org/post1",
        "id": "http://example.org/anno1",
        "target": "http://example.com/page1",
        "type": "Annotation",
    }
    aclient.post(f"/{slug}", json=data)
    response = client.get(f"/{slug}")
    json = response.json()
    assert json["total"] > 0
    with pytest.raises(CollectionNotEmpty):
        client.delete(f"/{slug}", headers={"if-match": response.headers["etag"]})


def test_delete_with_match_etag():
    # first create and then delete
    slug = "createdelete"
    response = client.post("/", headers={"slug": slug})
    delete_response = client.delete(
        f"/{slug}", headers={"if-match": response.headers["etag"]}
    )
    assert delete_response.status_code == 204


def test_options():
    slug = "existing"
    response = client.options(f"/{slug}")
    assert response.status_code == 204


def test_options_reserved():
    with pytest.raises(CollectionNameReserved):
        for collection_slug in RESERVED_COLLECTION_NAMES:
            client.options(f"/{collection_slug}")


def test_options_not_exists():
    collection_slug = "notexisting"
    with pytest.raises(CollectionNotFound):
        client.options(f"/{collection_slug}")


def test_head():
    slug = "existing"
    response = client.head(f"/{slug}")
    assert response.status_code == 204


def test_head_reserved():
    with pytest.raises(CollectionNameReserved):
        for collection_slug in RESERVED_COLLECTION_NAMES:
            client.head(f"/{collection_slug}")


def test_head_not_exists():
    collection_slug = "notexisting"
    with pytest.raises(CollectionNotFound):
        client.head(f"/{collection_slug}")


def test_get_after_post():
    collection_slug = "getafterpost"
    post_response = client.post("/", headers={"slug": collection_slug})
    get_response = client.get(f"/{collection_slug}/")
    # response body must be the same length,...
    assert (
        post_response.headers["content-length"]
        == get_response.headers["content-length"]
    )
    # ...the same content,...
    assert post_response.json() == get_response.json()
    # ...and thus the same etag
    assert post_response.headers["etag"] == get_response.headers["etag"]


def test_page_rendering():
    """If type annotations on EmbeddedPage.items are switched to `items:
    list[HttpUrlString] | list[EmbeddedAnnotation]`, a list of string values is returned
    instead of a list of Annotation items under unclear circumstances. This test
    prevents a regression. It is enough to test on collections and leave out page
    responses because both rely on the same model - that is EmbeddedPage.
    """
    collection_slug = "pagerendering"
    client.post("/", headers={"slug": collection_slug})
    data = {
        "@context": "http://www.w3.org/ns/anno.jsonld",
        "body": "http://example.org/post1",
        "id": "http://example.org/anno1",
        "target": "http://example.com/page1",
        "type": "Annotation",
    }
    aclient.post(f"/{collection_slug}", json=data)
    # defaults to embedded rendering of items, check for that
    response = client.get(f"/{collection_slug}/?page=0")
    json = response.json()
    assert len(json.get("items")) == 1
    # get the first item; it is not supposed to be a string
    assert not isinstance(json.get("items")[0], str)


def test_representation_independent_etag():
    collection_slug = "identicaletags"
    client.post("/", headers={"slug": collection_slug})
    data = {
        "@context": "http://www.w3.org/ns/anno.jsonld",
        "body": "http://example.org/post1",
        "id": "http://example.org/anno1",
        "target": "http://example.com/page1",
        "type": "Annotation",
    }
    aclient.post(f"/{collection_slug}", json=data)

    response_default = client.get(f"/{collection_slug}")
    etag_default = response_default.headers.get("etag")
    response_minimal = client.get(
        f"/{collection_slug}", headers={"Prefer": CONTAINER_PREFER_MINIMAL}
    )
    etag_minimal = response_minimal.headers.get("etag")
    response_iris = client.get(
        f"/{collection_slug}", headers={"Prefer": CONTAINER_PREFER_IRIS}
    )
    etag_iris = response_iris.headers.get("etag")
    response_desc = client.get(
        f"/{collection_slug}", headers={"Prefer": CONTAINER_PREFER_DESCRIPTIONS}
    )
    etag_desc = response_desc.headers.get("etag")

    assert etag_default == etag_minimal
    assert etag_default == etag_iris
    assert etag_default == etag_desc
