# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the Extensions API router."""


from fastapi.testclient import TestClient

from wapcas.extensions_router import router

client = TestClient(router)


def test_read_health():
    response = client.get("/healthz")
    assert response.status_code == 200
    assert response.json() == 1
