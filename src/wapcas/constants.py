# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: CC0-1.0

"""Constants of string literals."""

from typing import Final, LiteralString

# Names of the database collections for web annotation collections,
# web annotation pages, web annotations and views
COLLECTIONS_NAME: Final[str] = "__collections__"
PAGES_NAME: Final[str] = "__pages__"
ANNOTATIONS_NAME: Final[str] = "__annotations__"
COLLECTIONS_WITH_EMBEDDED_PAGES_NAME: Final[str] = "__collections_with_embedded_pages__"
COLLECTIONS_WITH_EMBEDDED_ANNOTATIONS_NAME: Final[str] = (
    "__collections_with_embedded_annotations__"
)
PAGES_WITH_EMBEDDED_ANNOTATIONS_NAME: Final[str] = "__pages_with_embedded_annotations__"

RESERVED_COLLECTION_NAMES: Final[list[str]] = ["_", "docs", "redoc"]
"""Collection names reserved for documentation and API extensions."""

RESERVED_CHARACTERS: Final[list[str]] = [
    "?",
    "#",
    "/",
    "=",
    ";",
    ":",
    "@",
    "&",
    "+",
    ",",
    "$",
]
"""Characters that may not be used to identify a collection or annotation to prevent
URL confusion attacks.
"""

OA_NAMESPACE_URI: Final[str] = "http://www.w3.org/ns/anno.jsonld"
LDP_NAMESPACE_URI: Final[str] = "http://www.w3.org/ns/ldp.jsonld"
# Field names and values
OA_CONTEXT_FIELD_ALIAS: Final[LiteralString] = "@context"
OA_CONTEXT_FIELD_NAME: Final[LiteralString] = "context"
OA_CONTEXT_FIELD_DEFAULT_VALUE: Final[str] = OA_NAMESPACE_URI
OA_TYPE_FIELD_NAME: Final[LiteralString] = "type"
OA_BODYVALUE_FIELD_NAME: Final[LiteralString] = "bodyValue"

OA_ANNOTATION_TYPE_NAME: Final[str] = "Annotation"
LDP_BASIC_CONTAINER_TYPE_NAME = "BasicContainer"
OA_COLLECTION_TYPE_NAME = "AnnotationCollection"
OA_PAGE_TYPE_NAME: Final[str] = "AnnotationPage"

# context field default values
OA_ANNOTATION_CONTEXT_FIELD_DEFAULT_VALUE: Final[str] = OA_NAMESPACE_URI
OA_COLLECTION_CONTEXT_FIELD_DEFAULT_VALUE: Final[list[str]] = [
    OA_NAMESPACE_URI,
    LDP_NAMESPACE_URI,
]
OA_PAGE_CONTEXT_FIELD_DEFAULT_VALUE: Final[str] = OA_NAMESPACE_URI
# type field default values
OA_ANNOTATION_TYPE_FIELD_DEFAULT_VALUE: Final[str] = OA_ANNOTATION_TYPE_NAME
OA_COLLECTION_TYPE_FIELD_DEFAULT_VALUE: Final[list[str]] = [
    OA_COLLECTION_TYPE_NAME,
    LDP_BASIC_CONTAINER_TYPE_NAME,
]
OA_PAGE_TYPE_FIELD_DEFAULT_VALUE: Final[str] = OA_PAGE_TYPE_NAME


JSON_LD_MIME_TYPE: Final[str] = "application/ld+json"
WEB_ANNOTATION_MIME_TYPE: Final[str] = (
    f"{JSON_LD_MIME_TYPE};profile='{OA_NAMESPACE_URI}'"
)

CONTAINER_LINK_TYPE: Final[str] = '<http://www.w3.org/ns/ldp#BasicContainer>;rel="type"'
CONTAINER_LINK_CONSTRAINT: Final[str] = (
    "<http://www.w3.org/TR/annotation-protocol/>;"
    'rel="http://www.w3.org/ns/ldp#constrainedBy"'
)

# Concerning representation preference, the OA vocabulary
# (https://www.w3.org/TR/annotation-vocab/) seems to be off track because it references
# terms in the ldp vocabulary (https://www.w3.org/ns/ldp) that just do not exist. This
# is why we will be very tolerant concerning the evaluation of the "Prefer"-Header
# against the specified values, i.e. we do only check against the URI fragment
# identifier.
CONTAINER_PREFER_MINIMAL: Final[str] = "PreferMinimalContainer"
CONTAINER_PREFER_IRIS: Final[str] = "PreferContainedIRIs"
CONTAINER_PREFER_DESCRIPTIONS: Final[str] = "PreferContainedDescriptions"


# "Allow"-header settings
ANNOTATION_ALLOW: Final[str] = (
    "OPTIONS, GET, HEAD, PUT, DELETE"  # everything except PATCH and POST
)
COLLECTION_ALLOW: Final[str] = "OPTIONS, GET, HEAD, POST, DELETE"  # no update
PAGE_ALLOW: Final[str] = "OPTIONS, GET, HEAD"  # read-only
ROOT_ALLOW: Final[str] = "OPTIONS, POST"  # create-only
# response constants (headers)
RESPONSE_CONTENT_TYPE = WEB_ANNOTATION_MIME_TYPE
RESPONSE_ACCEPT_POST = WEB_ANNOTATION_MIME_TYPE
