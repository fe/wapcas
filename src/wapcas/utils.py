# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: EUPL-1.2

"""WAPCAS Utilities."""

import datetime
import hashlib
import json
import os
from typing import Any, Final

from fastapi import Response, status
from fastapi.responses import JSONResponse
from pymongo.collection import Collection

from dbinit import db
from wapcas.constants import (
    ANNOTATIONS_NAME,
    COLLECTIONS_NAME,
    COLLECTIONS_WITH_EMBEDDED_ANNOTATIONS_NAME,
    COLLECTIONS_WITH_EMBEDDED_PAGES_NAME,
    PAGES_NAME,
    PAGES_WITH_EMBEDDED_ANNOTATIONS_NAME,
    RESPONSE_CONTENT_TYPE,
)

_BASE_URL = os.getenv("WAPCAS_BASE_URL", "http://localhost")
# Ensure BASE_URL does not end with "/".
BASE_URL: Final[str] = _BASE_URL if not _BASE_URL.endswith("/") else _BASE_URL[:-1]
"""The URL that is used as base for Object IDs."""
try:
    _page_size = int(os.getenv("WAPCAS_DEFAULT_PAGE_SIZE", "10"))
except ValueError:
    _page_size = 10
DEFAULT_PAGE_SIZE: Final[int] = _page_size
"""The default collection page size."""

# This is set to a low value for testing purposes during BETA development phase.
DEFAULT_DB_TIMEOUT: Final[float] = 1.0

# Database collections
COLLECTIONS: Final[Collection[dict[str, Any]]] = db[COLLECTIONS_NAME]
PAGES: Final[Collection[dict[str, Any]]] = db[PAGES_NAME]
ANNOTATIONS: Final[Collection[dict[str, Any]]] = db[ANNOTATIONS_NAME]

# Database views
PAGES_VIEW: Final[Collection[dict[str, Any]]] = db[COLLECTIONS_WITH_EMBEDDED_PAGES_NAME]
ANNOTATIONS_VIEW: Final[Collection[dict[str, Any]]] = db[
    COLLECTIONS_WITH_EMBEDDED_ANNOTATIONS_NAME
]
PAGES_WITH_ANNOTATIONS_VIEW: Final[Collection[dict[str, Any]]] = db[
    PAGES_WITH_EMBEDDED_ANNOTATIONS_NAME
]

# Common options for responses with empty body.
EMPTY_RESPONSE_OPTIONS: Final[dict[str, Any]] = {
    "response_class": Response,
    "response_description": "Empty response.",
    "status_code": status.HTTP_204_NO_CONTENT,
}


def isotimestamp() -> str:
    """Function shortcut alias for getting an iso formatted timestamp."""
    return datetime.datetime.now(datetime.timezone.utc).isoformat()


def create_etag(input_data: dict[str, Any]) -> str:
    """Creates a hexadecimal digest of an input-object's SHA256 hash to be used as an
    ETAG.

    Args:
        input_data (dict[str, Any]): Input data.

    Returns:
        str: ETAG
    """
    return hashlib.sha256(
        json.dumps(input_data).encode(), usedforsecurity=False
    ).hexdigest()


class JSONLDResponse(JSONResponse):
    """Class with modified `media_type` based on JSONResponse to comply with WAP."""

    media_type = RESPONSE_CONTENT_TYPE
