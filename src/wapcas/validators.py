# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: EUPL-1.2

"""Custom Pydantic validators for data validation by FastAPI."""

from typing import Annotated

from pydantic import AfterValidator, HttpUrl

HttpUrlString = Annotated[HttpUrl, AfterValidator(str)]
"""Type alias that casts the HttpUrl type to a str after validation."""


def value_of_key_contains_item(
    key: str, value: str | list[str] | HttpUrlString | list[HttpUrlString], item: str
):
    """Generic validator that checks whether a value is a list that contains a specific
    item or is the item.

    Args:
        key (str): Name of the key in the pydantic model to check the value for
        value (str | list[str] | HttpUrlString | list[HttpUrlString]): The actual value
        item (str): The item to check against

    Raises:
        ValueError: if the item is not found in the list or value is not the item

    Returns:
        str | list[str] | HttpUrlString | list[HttpUrlString]: \
            value after validation succeeded
    """
    if item not in (value if isinstance(value, list) else [value]):
        raise ValueError(f"'{key}' must at least contain '{item}'")
    return value
