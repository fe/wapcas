# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: EUPL-1.2

"""Custom Pydantic serializers for data serialization by FastAPI."""
from pydantic import HttpUrl


def serialize_single_context_or_list(context: HttpUrl | list[HttpUrl]):
    """This method allows a context to be serialized either to a string or a list of
    strings, according to its input.
    """
    return [str(url) for url in context] if isinstance(context, list) else str(context)
