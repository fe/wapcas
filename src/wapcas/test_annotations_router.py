# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the Annotations API router."""

import json as jsonlib
import uuid
from glob import glob
from typing import Any

import pytest
from fastapi.testclient import TestClient

from wapcas.annotations_router import router
from wapcas.collections_router import router as crouter
from wapcas.constants import RESERVED_COLLECTION_NAMES
from wapcas.exceptions import (
    AnnotationExists,
    AnnotationMismatch,
    AnnotationNotFound,
    CollectionNameReserved,
    CollectionNotFound,
    ReservedCharacterUsedInIdentifier,
)
from wapcas.utils import BASE_URL, DEFAULT_PAGE_SIZE

# prepare a collection to test in
pclient = TestClient(crouter)
COLLECTION_SLUG = "annotation_tests_collection"
pclient.post("/", headers={"slug": COLLECTION_SLUG})

client = TestClient(router)

DATA = {
    "@context": "http://www.w3.org/ns/anno.jsonld",
    "body": "http://example.org/post1",
    "id": "http://example.org/anno1",
    "target": "http://example.com/page1",
    "type": "Annotation",
}


def test_create_annotation():
    response = client.post(f"/{COLLECTION_SLUG}/", json=DATA)
    json = response.json()
    assert response.status_code == 201
    assert json["@context"] == DATA["@context"]
    assert DATA["type"] in json["type"]
    assert json["id"].startswith(f"{BASE_URL}/{COLLECTION_SLUG}/")
    assert json["via"] == DATA["id"]
    assert response.headers["location"] == json["id"]


def test_create_annotation_without_id():
    data = {
        "@context": "http://www.w3.org/ns/anno.jsonld",
        "body": "http://example.org/post1",
        "target": "http://example.com/page1",
        "type": "Annotation",
    }
    response = client.post(f"/{COLLECTION_SLUG}/", json=data)
    json = response.json()
    assert response.status_code == 201
    assert json["@context"] == data["@context"]
    assert data["type"] in json["type"]
    assert json["id"].startswith(f"{BASE_URL}/{COLLECTION_SLUG}/")
    # generated id must end in a valid UUID, raises if invalid
    uuid.UUID(json["id"].split("/")[-1], version=4)
    with pytest.raises(KeyError):
        json["via"]  # pylint: disable=pointless-statement


def test_create_annotation_on_a_new_page():
    collection_slug = "emptyCollection"
    # create an empty collection
    pclient.post("/", headers={"slug": collection_slug})
    for _ in range(DEFAULT_PAGE_SIZE):
        client.post(f"/{collection_slug}/", json=DATA)
    # get the collection with a full page
    response_before = pclient.get(f"/{collection_slug}/")
    assert response_before.status_code == 200
    json_before = response_before.json()
    assert json_before["total"] == DEFAULT_PAGE_SIZE
    # the next line will also fail if the serialization of the first page is string instead of EmbeddedPage
    assert json_before["first"]["id"] == json_before["last"]
    # get the 1st page
    first_page_response_before = pclient.get(f"/{collection_slug}/?page=0")
    assert first_page_response_before.status_code == 200
    first_page_json_before = first_page_response_before.json()
    assert first_page_json_before.get("partOf") == json_before.get("id")
    assert first_page_json_before.get("startIndex") == 0
    assert len(first_page_json_before.get("items")) == DEFAULT_PAGE_SIZE
    assert first_page_json_before.get("next") is None
    assert first_page_json_before.get("prev") is None
    # this is the 11th annotation
    client.post(f"/{collection_slug}/", json=DATA)
    reseponse_after = pclient.get(f"/{collection_slug}/")
    assert reseponse_after.status_code == 200
    json_after = reseponse_after.json()
    assert json_after["total"] == DEFAULT_PAGE_SIZE + 1
    assert json_after["first"]["id"] != json_after["last"]
    # get the 1st page after creation of the 2nd
    first_page_response_after = pclient.get(f"/{collection_slug}/?page=0")
    assert first_page_response_after.status_code == 200
    first_page_json_after = first_page_response_after.json()
    assert first_page_json_after.get("partOf") == json_before.get("id")
    assert first_page_json_after.get("startIndex") == 0
    assert len(first_page_json_after.get("items")) == DEFAULT_PAGE_SIZE
    # has a next page now
    assert first_page_json_after.get("next") is not None
    assert first_page_json_after.get("prev") is None
    # get the 2nd page
    second_page_response = pclient.get(f"/{collection_slug}/?page=1")
    assert second_page_response.status_code == 200
    second_page_json = second_page_response.json()
    assert second_page_json.get("id") == first_page_json_after.get("next")
    assert second_page_json.get("partOf") == json_before.get("id")
    assert second_page_json.get("startIndex") == DEFAULT_PAGE_SIZE
    assert len(second_page_json.get("items")) == 1
    assert second_page_json.get("next") is None
    assert second_page_json.get("prev") == first_page_json_after.get("id")


def test_create_reserved():
    with pytest.raises(CollectionNameReserved):
        for collection_slug in RESERVED_COLLECTION_NAMES:
            client.post(f"/{collection_slug}/", json=DATA)


def test_create_collection_not_exists():
    collection_slug = "notexisting"
    with pytest.raises(CollectionNotFound):
        client.post(f"/{collection_slug}/", json=DATA)


def test_create_annotation_exists():
    slug = "existing"
    client.post(f"/{COLLECTION_SLUG}/", json=DATA, headers={"slug": slug})
    with pytest.raises(AnnotationExists):
        client.post(f"/{COLLECTION_SLUG}/", json=DATA, headers={"slug": slug})


def test_retrieve_annotation():
    slug = "existing"
    response = client.get(f"/{COLLECTION_SLUG}/{slug}")
    json = response.json()
    assert response.status_code == 200
    assert json["id"] == (f"{BASE_URL}/{COLLECTION_SLUG}/{slug}")


def test_retrieve_annotation_not_exists():
    slug = "not_existing"
    with pytest.raises(AnnotationNotFound):
        client.get(f"/{COLLECTION_SLUG}/{slug}")


def test_retrieve_reserved():
    slug = "existing"
    with pytest.raises(CollectionNameReserved):
        for collection_slug in RESERVED_COLLECTION_NAMES:
            client.get(f"/{collection_slug}/{slug}")


def test_create_query():
    slug = "query?"
    with pytest.raises(ReservedCharacterUsedInIdentifier):
        client.post(f"/{COLLECTION_SLUG}/", json=DATA, headers={"slug": slug})


def test_create_fragment():
    slug = "fragment#"
    with pytest.raises(ReservedCharacterUsedInIdentifier):
        client.post(f"/{COLLECTION_SLUG}/", json=DATA, headers={"slug": slug})


def test_create_slash():
    slug = "slash/"
    with pytest.raises(ReservedCharacterUsedInIdentifier):
        client.post(f"/{COLLECTION_SLUG}/", json=DATA, headers={"slug": slug})


def test_retrieve_collection_not_exists():
    collection_slug = "notexisting"
    slug = "existing"
    with pytest.raises(CollectionNotFound):
        client.get(f"/{collection_slug}/{slug}")


def test_update_collection_not_exists():
    collection_slug = "notexisting"
    slug = "existing"
    with pytest.raises(CollectionNotFound):
        client.put(f"/{collection_slug}/{slug}", json=DATA)


def test_update_annotation_not_exists():
    slug = "not_existing"
    with pytest.raises(AnnotationNotFound):
        client.put(f"/{COLLECTION_SLUG}/{slug}", json=DATA)


def test_update_reserved():
    slug = "existing"
    with pytest.raises(CollectionNameReserved):
        for collection_slug in RESERVED_COLLECTION_NAMES:
            client.put(f"/{collection_slug}/{slug}", json=DATA)


def test_update_annotation_no_etag():
    slug = "existing"
    with pytest.raises(AnnotationMismatch):
        client.put(f"/{COLLECTION_SLUG}/{slug}", json=DATA)


def test_delete_collection_not_exists():
    collection_slug = "notexisting"
    slug = "existing"
    with pytest.raises(CollectionNotFound):
        client.delete(f"/{collection_slug}/{slug}")


def test_delete_annotation_not_exists():
    slug = "not_existing"
    with pytest.raises(AnnotationNotFound):
        client.delete(f"/{COLLECTION_SLUG}/{slug}")


def test_delete_reserved():
    slug = "existing"
    with pytest.raises(CollectionNameReserved):
        for collection_slug in RESERVED_COLLECTION_NAMES:
            client.delete(f"/{collection_slug}/{slug}")


def test_delete_no_etag():
    slug = "existing"
    with pytest.raises(AnnotationMismatch):
        client.delete(f"/{COLLECTION_SLUG}/{slug}")


def test_options():
    slug = "existing"
    response = client.options(f"/{COLLECTION_SLUG}/{slug}")
    assert response.status_code == 204


def test_options_collection_not_exists():
    collection_slug = "notexisting"
    slug = "existing"
    with pytest.raises(CollectionNotFound):
        client.options(f"/{collection_slug}/{slug}")


def test_options_annotation_not_exists():
    slug = "not_existing"
    with pytest.raises(AnnotationNotFound):
        client.options(f"/{COLLECTION_SLUG}/{slug}")


def test_options_reserved():
    slug = "existing"
    with pytest.raises(CollectionNameReserved):
        for collection_slug in RESERVED_COLLECTION_NAMES:
            client.options(f"/{collection_slug}/{slug}")


def test_head():
    slug = "existing"
    response = client.head(f"/{COLLECTION_SLUG}/{slug}")
    assert response.status_code == 204


def test_head_collection_not_exists():
    collection_slug = "notexisting"
    slug = "existing"
    with pytest.raises(CollectionNotFound):
        client.head(f"/{collection_slug}/{slug}")


def test_head_annotation_not_exists():
    slug = "not_existing"
    with pytest.raises(AnnotationNotFound):
        client.head(f"/{COLLECTION_SLUG}/{slug}")


def test_head_reserved():
    slug = "existing"
    with pytest.raises(CollectionNameReserved):
        for collection_slug in RESERVED_COLLECTION_NAMES:
            client.head(f"/{collection_slug}/{slug}")


def test_crud_etags_match():
    """Create/Retrieve and Update/Retrieve must have matching etags."""
    slug = "create_update"
    create_response = client.post(
        f"/{COLLECTION_SLUG}/", json=DATA, headers={"slug": slug}
    )
    retrieve_after_create_response = client.get(f"/{COLLECTION_SLUG}/{slug}")
    assert (
        create_response.headers["etag"]
        == retrieve_after_create_response.headers["etag"]
    )
    new_data = {
        "@context": "http://www.w3.org/ns/anno.jsonld",
        "body": "http://example.org/haschanged",
        "id": "http://example.org/anno1",
        "target": "http://example.com/hasalsochanged",
        "type": "Annotation",
    }
    update_response = client.put(
        f"/{COLLECTION_SLUG}/{slug}",
        json=new_data,
        headers={"if-match": create_response.headers["etag"]},
    )
    retrieve_after_update_response = client.get(f"/{COLLECTION_SLUG}/{slug}")
    assert (
        retrieve_after_create_response.headers["etag"]
        != retrieve_after_update_response.headers["etag"]
    )
    assert (
        update_response.headers["etag"]
        == retrieve_after_update_response.headers["etag"]
    )
    delete_updated_response = client.delete(
        f"/{COLLECTION_SLUG}/{slug}",
        headers={"if-match": retrieve_after_update_response.headers["etag"]},
    )
    assert delete_updated_response.status_code == 204
    with pytest.raises(AnnotationNotFound):
        client.get(f"/{COLLECTION_SLUG}/{slug}")


def test_replace_existing_with_different_id():
    """Replacing an annotation with a different id field value MUST override the "via"
    key in the new data but not the id field.
    """
    slug = "with_different_id"
    create_response = client.post(
        f"/{COLLECTION_SLUG}/", json=DATA, headers={"slug": slug}
    )
    create_response_json = create_response.json()

    new_data = {
        "@context": "http://www.w3.org/ns/anno.jsonld",
        "body": "http://example.org/yetanotherpost",
        "id": "http://example.org/verydifferentid",
        "target": "http://example.com/page1",
        "type": "Annotation",
        "via": create_response_json["via"],
    }

    update_response = client.put(
        f"/{COLLECTION_SLUG}/{slug}",
        json=new_data,
        headers={"if-match": create_response.headers["etag"]},
    )
    update_response_json = update_response.json()
    assert create_response_json["id"] == update_response_json["id"]
    assert new_data["id"] == update_response_json["via"]


def test_create_update_modified_in_collection():
    """Creating an annotation MUST update the "modified" field in the parent
    collection.
    """
    slug = "create_annotation_update_modified"
    create_response = client.post(
        f"/{COLLECTION_SLUG}/", json=DATA, headers={"slug": slug}
    )
    create_json = create_response.json()
    retrieve_response = pclient.get(f"/{COLLECTION_SLUG}/")
    retrieve_json = retrieve_response.json()
    # on create, both modified and created MUST be the same
    assert retrieve_json["modified"] == create_json["created"]
    assert retrieve_json["modified"] == create_json["modified"]


def test_replace_update_modified_in_collection():
    """Replacing an annotation MUST update the "modified" field in the parent
    collection.
    """
    slug = "replace_annotation_update_modified"
    create_response = client.post(
        f"/{COLLECTION_SLUG}/", json=DATA, headers={"slug": slug}
    )
    update_response = client.put(
        f"/{COLLECTION_SLUG}/{slug}",
        json=create_response.json(),
        headers={"if-match": create_response.headers["etag"]},
    )
    update_json = update_response.json()
    retrieve_response = pclient.get(f"/{COLLECTION_SLUG}/")
    retrieve_json = retrieve_response.json()
    # since this is replace, both modified and created MUST be the same
    assert retrieve_json["modified"] == update_json["created"]
    assert retrieve_json["modified"] == update_json["modified"]


# complete example test now part of the following fixtures
files = glob("src/fixtures/*.json")
"""Test files from the [web platform tests for the annotation-
protocol](https://github.com/web-platform-tests/wpt/tree/master/annotation-
protocol/files/annotations). Numbers 9, 11, 12, and 13 were removed because they test
against non-normative properties. Also, jsonld files 1, 2, and 3 were removed because of
non-URL ids.
"""


@pytest.fixture(params=files, name="annotation_example")
def fixture_annotation_example(request: pytest.FixtureRequest):
    with open(request.param, encoding="UTF-8") as file:
        return jsonlib.loads(file.read())


def test_fixture(annotation_example: Any):
    response = client.post(f"/{COLLECTION_SLUG}/", json=annotation_example)
    json = response.json()
    assert response.status_code == 201
    assert json["@context"] == annotation_example["@context"]
    assert annotation_example["type"] in json["type"]
    assert json["id"].startswith(f"{BASE_URL}/{COLLECTION_SLUG}/")
    assert json["via"] == annotation_example["id"]
    assert response.headers["location"] == json["id"]
