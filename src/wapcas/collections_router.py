# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: EUPL-1.2

"""Annotation Collections CR(U)D API Router."""

import uuid
from typing import Annotated, Any, Final, Union

from fastapi import APIRouter, Header, status
from fastapi.responses import Response
from pydantic import HttpUrl, NonNegativeInt
from pymongo import timeout
from pymongo.errors import ServerSelectionTimeoutError

from wapcas import models
from wapcas.constants import (
    COLLECTION_ALLOW,
    CONTAINER_LINK_CONSTRAINT,
    CONTAINER_LINK_TYPE,
    CONTAINER_PREFER_DESCRIPTIONS,
    CONTAINER_PREFER_IRIS,
    PAGE_ALLOW,
    RESERVED_CHARACTERS,
    RESERVED_COLLECTION_NAMES,
    RESPONSE_ACCEPT_POST,
    RESPONSE_CONTENT_TYPE,
    ROOT_ALLOW,
)
from wapcas.exceptions import (
    CollectionExists,
    CollectionMismatch,
    CollectionNameReserved,
    CollectionNotEmpty,
    CollectionNotFound,
    DatabaseTimeout,
    PageNotFound,
    ReservedCharacterUsedInIdentifier,
)
from wapcas.models import DEFAULT_MODEL_DUMP_OPTIONS
from wapcas.utils import (
    ANNOTATIONS_VIEW,
    BASE_URL,
    COLLECTIONS,
    DEFAULT_DB_TIMEOUT,
    EMPTY_RESPONSE_OPTIONS,
    PAGES,
    PAGES_VIEW,
    PAGES_WITH_ANNOTATIONS_VIEW,
    JSONLDResponse,
    create_etag,
    isotimestamp,
)

router = APIRouter()


@router.post(
    "/",
    responses={
        status.HTTP_403_FORBIDDEN: {"model": models.ErrorMessage},
        status.HTTP_405_METHOD_NOT_ALLOWED: {"model": models.ErrorMessage},
        status.HTTP_409_CONFLICT: {"model": models.ErrorMessage},
        status.HTTP_503_SERVICE_UNAVAILABLE: {"model": models.ErrorMessage},
    },
    response_class=JSONLDResponse,
    response_description="The created Annotation Collection.",
    response_model=models.Collection,
    response_model_exclude_none=True,
    status_code=status.HTTP_201_CREATED,
    summary="Create a Web Annotation Collection.",
    tags=["collections"],
)
def create_collection(
    response: JSONLDResponse,
    slug: str | None = Header(default=None),
) -> models.Collection:
    """
    Create a Web Annotation Collection with an optional
    `slug` for the Collection ID and return it.
    """

    if slug and any(char in slug for char in RESERVED_CHARACTERS):
        raise ReservedCharacterUsedInIdentifier(
            slug=slug, allow_header=COLLECTION_ALLOW
        )

    if slug in RESERVED_COLLECTION_NAMES:
        raise CollectionNameReserved()

    new_id = slug or str(uuid.uuid4())

    COLLECTION_ID: Final[str] = f"{BASE_URL}/{new_id}/"

    collection_document = COLLECTIONS.find_one({"id": COLLECTION_ID}, {"_id": False})
    if collection_document:
        raise CollectionExists(new_id, COLLECTION_ID)

    timestamp: Final[str] = isotimestamp()

    new_collection = models.Collection(
        id=HttpUrl(COLLECTION_ID),
        created=timestamp,
        modified=timestamp,
    )
    # create etag from the model instance dumped into a dict
    etag = create_etag(new_collection.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS))

    # dump directly to avoid having to pop the unserializable "_id" key
    # that is added by insert_one
    COLLECTIONS.insert_one(new_collection.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS))

    response.headers["Allow"] = COLLECTION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    # preserve duplicate "Link" header by using append
    response.headers.append("Link", CONTAINER_LINK_TYPE)
    response.headers.append("Link", CONTAINER_LINK_CONSTRAINT)
    response.headers["Etag"] = etag
    response.headers["Location"] = COLLECTION_ID
    response.status_code = status.HTTP_201_CREATED

    return new_collection


retrieve_collection_options: Final[dict[str, Any]] = {
    "responses": {
        status.HTTP_404_NOT_FOUND: {"model": models.ErrorMessage},
        status.HTTP_405_METHOD_NOT_ALLOWED: {"model": models.ErrorMessage},
        status.HTTP_503_SERVICE_UNAVAILABLE: {"model": models.ErrorMessage},
    },
    "response_class": JSONLDResponse,
    "response_description": "The requested Annotation Collection or Annotation Page.",
    "response_model": Union[models.Page, models.Collection],
    "response_model_exclude_none": True,
    "summary": "Retrieve an Annotation Collection or Annotation Page.",
    "tags": ["collections"],
}


@router.get(
    "/{collection_slug}/",
    **retrieve_collection_options,
)
@router.get(
    "/{collection_slug}",
    **retrieve_collection_options,
    include_in_schema=False,
)
def retrieve_collection(
    collection_slug: str,
    response: JSONLDResponse,
    page: int | None = None,
    prefer: Annotated[str | None, Header()] = None,
) -> Any:
    """Retrieve an Annotation Collection (`collection_slug`) or an Annotation
    Page (`page`).
    """

    if collection_slug in RESERVED_COLLECTION_NAMES:
        raise CollectionNameReserved()

    COLLECTION_ID: Final[str] = f"{BASE_URL}/{collection_slug}/"
    db_collection = COLLECTIONS
    try:
        with timeout(DEFAULT_DB_TIMEOUT):
            collection_document = db_collection.find_one(
                {"id": COLLECTION_ID}, {"_id": False}
            )
    # this is only one of the possible timeout errors,
    # see https://pymongo.readthedocs.io/en/stable/examples/timeouts.html#timeout-errors
    # and following
    except ServerSelectionTimeoutError as e:
        raise DatabaseTimeout() from e

    if not collection_document:
        raise CollectionNotFound(collection_slug, COLLECTION_ID)

    # set headers shared by Collection and Page
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    response.headers["Vary"] = "Accept,Prefer"

    if page is not None:
        return return_page(
            collection_slug=collection_slug,
            page_number=page,
            preference=prefer,
            response=response,
        )

    collection = models.Collection(**collection_document)
    # Create a representation-independent ETAG
    etag = create_etag(collection.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS))

    has_annotations = collection.total > 0
    # collection has minimal container in the db but default return is contained
    # descriptions must check for annotations because cannot provide other view than
    # minimal for an empty collection
    if has_annotations:
        if not prefer or prefer.find(CONTAINER_PREFER_DESCRIPTIONS) != -1:
            db_collection = ANNOTATIONS_VIEW

        if prefer and prefer.find(CONTAINER_PREFER_IRIS) != -1:
            db_collection = PAGES_VIEW

        collection_document = db_collection.find_one(
            {"id": COLLECTION_ID}, {"_id": False}
        )
        # This should actually never occur, but...who knows.
        if not collection_document:
            raise CollectionNotFound(collection_slug, COLLECTION_ID)
        # now, overwrite the minimal collection
        collection = models.Collection(**collection_document)

    response.headers["Accept-Post"] = RESPONSE_ACCEPT_POST
    response.headers["Allow"] = COLLECTION_ALLOW
    response.headers["Etag"] = etag
    # preserve duplicate "Link" header by using append
    response.headers.append("Link", CONTAINER_LINK_TYPE)
    response.headers.append("Link", CONTAINER_LINK_CONSTRAINT)
    # It is questionable whether this header MUST be set.
    # see https://gitlab.gwdg.de/fe/wapcas/-/issues/9
    # response.headers["Content-Location"] = COLLECTION_ID

    return collection


delete_collection_options: Final[dict[str, Any]] = {
    "responses": {
        status.HTTP_404_NOT_FOUND: {"model": models.ErrorMessage},
        status.HTTP_405_METHOD_NOT_ALLOWED: {"model": models.ErrorMessage},
        status.HTTP_409_CONFLICT: {"model": models.ErrorMessage},
        status.HTTP_412_PRECONDITION_FAILED: {"model": models.ErrorMessage},
    },
    **EMPTY_RESPONSE_OPTIONS,
    "summary": "Delete an Annotation Collection.",
    "tags": ["collections"],
}


@router.delete(
    "/{collection_slug}/",
    **delete_collection_options,
)
@router.delete(
    "/{collection_slug}",
    **delete_collection_options,
    include_in_schema=False,
)
def delete_collection(
    collection_slug: str,
    if_match: Annotated[str | None, Header()] = None,
) -> None:
    """Delete an Annotation Collection (`collection_id`)."""

    if collection_slug in RESERVED_COLLECTION_NAMES:
        raise CollectionNameReserved()

    COLLECTION_ID: str = f"{BASE_URL}/{collection_slug}/"
    collection_document = COLLECTIONS.find_one({"id": COLLECTION_ID}, {"_id": False})
    if not collection_document:
        raise CollectionNotFound(collection_slug, COLLECTION_ID)

    # allow deletion of empty collections only (discussable!)
    if collection_document["total"] > 0:
        raise CollectionNotEmpty(collection_slug, COLLECTION_ID)

    etag = create_etag(collection_document)
    if if_match != etag:
        raise CollectionMismatch(collection_slug, COLLECTION_ID)

    # delete collection metadata document
    COLLECTIONS.delete_one({"id": COLLECTION_ID})


@router.options(
    "/",
    **EMPTY_RESPONSE_OPTIONS,
    tags=["collections"],
)
# pylint: disable=missing-function-docstring
def options_root(response: Response) -> None:
    response.headers["Allow"] = ROOT_ALLOW


empty_collection_response_options: Final[dict[str, Any]] = {
    "responses": {
        status.HTTP_404_NOT_FOUND: {"model": models.ErrorMessage},
        status.HTTP_405_METHOD_NOT_ALLOWED: {"model": models.ErrorMessage},
    },
    **EMPTY_RESPONSE_OPTIONS,
    "tags": ["collections"],
}


@router.options(
    "/{collection_slug}/",
    **empty_collection_response_options,
)
@router.options(
    "/{collection_slug}",
    **empty_collection_response_options,
    include_in_schema=False,
)
# pylint: disable=missing-function-docstring
def options_collection(collection_slug: str, response: Response) -> None:
    if collection_slug in RESERVED_COLLECTION_NAMES:
        raise CollectionNameReserved()
    COLLECTION_ID: str = f"{BASE_URL}/{collection_slug}/"
    collection_document = COLLECTIONS.find_one({"id": COLLECTION_ID}, {"_id": False})
    if not collection_document:
        raise CollectionNotFound(collection_slug, COLLECTION_ID)
    response.headers["Allow"] = COLLECTION_ALLOW


@router.head(
    "/{collection_slug}/",
    **empty_collection_response_options,
)
@router.head(
    "/{collection_slug}",
    **empty_collection_response_options,
    include_in_schema=False,
)
# pylint: disable=missing-function-docstring
def head_collection(collection_slug: str, response: Response) -> None:
    if collection_slug in RESERVED_COLLECTION_NAMES:
        raise CollectionNameReserved()
    COLLECTION_ID: Final[str] = f"{BASE_URL}/{collection_slug}/"
    collection_document = COLLECTIONS.find_one({"id": COLLECTION_ID}, {"_id": False})
    if not collection_document:
        raise CollectionNotFound(collection_slug, COLLECTION_ID)
    etag = create_etag(collection_document)
    response.headers["Etag"] = etag
    response.headers["Vary"] = "Accept,Prefer"
    response.headers["Allow"] = COLLECTION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    response.headers.append("Link", CONTAINER_LINK_TYPE)
    response.headers.append("Link", CONTAINER_LINK_CONSTRAINT)
    response.headers["Accept-Post"] = RESPONSE_ACCEPT_POST


def return_page(
    collection_slug: str,
    page_number: NonNegativeInt,
    preference: str | None,
    response: Response,
) -> models.Page:
    """Return an Annotation Page.

    Args:
        collection_slug (str): Slug of the page’s parent collection
        page_number (NonNegativeInt): 0-indexed page number
        preference (str): "Prefer"-header value
        response (Response): FastApi response object

    Raises:
        PageNotFound: Page not found

    Returns:
        Page: The requested Annotation Page
    """

    PAGE_ID: Final[str] = f"{BASE_URL}/{collection_slug}/?page={page_number}"
    # According to the specification of the WAP, the linked pages SHOULD follow the
    # Container's preference. However, this implementation diverges from this
    # recommendation in favor of stable page URIs.
    db_collection = PAGES_WITH_ANNOTATIONS_VIEW
    if preference and preference.find(CONTAINER_PREFER_IRIS) != -1:
        db_collection = PAGES
    page_document = db_collection.find_one({"id": PAGE_ID}, {"_id": False})

    if page_document is not None:
        validated_page = models.Page(**page_document)
        response.headers["Allow"] = PAGE_ALLOW
        return validated_page

    raise PageNotFound(page_number, PAGE_ID)
