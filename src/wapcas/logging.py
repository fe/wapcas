# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: CC0-1.0

"""Logging helpers and configurations."""

import logging
import os
from typing import Final

LOG_LEVEL: Final[str] = os.getenv("WAPCAS_LOG_LEVEL", "INFO")
"""The basic log level."""

def init():
    """Initialize logging configuration."""
    logging.basicConfig(
        level=(LOG_LEVEL if LOG_LEVEL in logging.getLevelNamesMapping() else "INFO"),
        format="%(asctime)s | %(name)s | %(levelname)s | %(message)s",
    )
    # Add a filter to uvicorns access logger to prevent spamming of logs by probes.
    logging.getLogger("uvicorn.access").addFilter(HealthCheckFilter())
    logging.logProcesses = False
    logging.logThreads = False


class HealthCheckFilter(logging.Filter):
    """Filter out health check access logs."""

    def filter(self, record: logging.LogRecord) -> bool:
        """Determine if the specified record is to be logged."""
        return record.getMessage().find("/healthz") == -1
