# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: EUPL-1.2

"""Web Annotations CRUD API Router."""

import uuid
from typing import Annotated, Any, Final

from fastapi import APIRouter, Header, status
from fastapi.responses import Response
from pydantic import HttpUrl
from pymongo import ReturnDocument
from pymongo.client_session import ClientSession
from pymongo.errors import InvalidOperation

from dbinit import mongo_client
from wapcas import models
from wapcas.constants import (
    ANNOTATION_ALLOW,
    OA_BODYVALUE_FIELD_NAME,
    RESERVED_CHARACTERS,
    RESERVED_COLLECTION_NAMES,
    RESPONSE_CONTENT_TYPE,
)
from wapcas.exceptions import (
    AnnotationExists,
    AnnotationMismatch,
    AnnotationNotFound,
    CollectionNameReserved,
    CollectionNotFound,
    DatabaseTransactionAbort,
    PageNotFound,
    ReservedCharacterUsedInIdentifier,
)
from wapcas.models import DEFAULT_MODEL_DUMP_OPTIONS
from wapcas.utils import (
    ANNOTATIONS,
    BASE_URL,
    COLLECTIONS,
    DEFAULT_PAGE_SIZE,
    EMPTY_RESPONSE_OPTIONS,
    PAGES,
    JSONLDResponse,
    create_etag,
    isotimestamp,
)

router = APIRouter()

create_annotation_options: Final[dict[str, Any]] = {
    "responses": {
        status.HTTP_403_FORBIDDEN: {"model": models.ErrorMessage},
        status.HTTP_404_NOT_FOUND: {"model": models.ErrorMessage},
        status.HTTP_405_METHOD_NOT_ALLOWED: {"model": models.ErrorMessage},
        status.HTTP_409_CONFLICT: {"model": models.ErrorMessage},
        status.HTTP_500_INTERNAL_SERVER_ERROR: {"model": models.ErrorMessage},
    },
    "response_class": JSONLDResponse,
    "response_description": "The created Annotation.",
    "response_model": models.Annotation,
    "response_model_exclude_none": True,
    "status_code": status.HTTP_201_CREATED,
    "summary": "Create a Web Annotation.",
    "tags": ["annotations"],
}


@router.post("/{collection_slug}/", **create_annotation_options)
@router.post(
    "/{collection_slug}",
    **create_annotation_options,
    include_in_schema=False,
)
def create_annotation(
    collection_slug: str,
    annotation: models.InputAnnotation,
    response: JSONLDResponse,
    slug: str | None = Header(default=None),
) -> models.Annotation:
    """
    Create a Web Annotation in a Collection (`collection_slug`) with an optional
    `slug` for the Annotation ID and return it.
    """

    if slug and any(char in slug for char in RESERVED_CHARACTERS):
        raise ReservedCharacterUsedInIdentifier(
            slug=slug, allow_header=ANNOTATION_ALLOW
        )

    if collection_slug in RESERVED_COLLECTION_NAMES:
        raise CollectionNameReserved()

    COLLECTION_ID: Final[str] = f"{BASE_URL}/{collection_slug}/"
    collection_document = COLLECTIONS.find_one({"id": COLLECTION_ID}, {"_id": False})
    if not collection_document:
        raise CollectionNotFound(collection_slug, COLLECTION_ID)

    new_slug = slug or str(uuid.uuid4())
    ANNOTATION_ID: Final[str] = COLLECTION_ID + new_slug

    annotation_document = ANNOTATIONS.find_one({"id": ANNOTATION_ID}, {"_id": False})
    if annotation_document:
        raise AnnotationExists(new_slug, ANNOTATION_ID)

    timestamp: str = isotimestamp()

    annotation_dict = annotation.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS)
    if "id" in annotation_dict:
        annotation_dict["via"] = annotation_dict["id"]

    annotation_dict["id"] = ANNOTATION_ID
    annotation_dict["created"] = timestamp
    annotation_dict["modified"] = timestamp

    # If there is a "bodyValue" property in the annotation, rewrite it to use the
    # TextualBody construction.
    if OA_BODYVALUE_FIELD_NAME in annotation_dict:
        annotation_dict["body"] = {
            "type": "TextualBody",
            "value": annotation_dict[OA_BODYVALUE_FIELD_NAME],
            "format": "text/plain",
        }
        del annotation_dict[OA_BODYVALUE_FIELD_NAME]

    new_annotation = models.Annotation(**annotation_dict)

    # have all write operations in a single transaction
    with mongo_client.start_session() as session:
        try:
            session.start_transaction()
        except InvalidOperation as e:
            raise DatabaseTransactionAbort(reason=str(e)) from e
        try:
            ANNOTATIONS.insert_one(
                new_annotation.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS),
                session=session,
            )
            collection_last = update_or_create_page(
                str(new_annotation.id),
                collection_document,
                session,
            )

            # update "modified" and "total" in the collection document
            COLLECTIONS.update_one(
                {"id": COLLECTION_ID},
                {
                    "$set": {"modified": timestamp, "last": collection_last},
                    "$inc": {"total": 1},
                },
                session=session,
            )
        except Exception as e:
            # Abort transaction if any error occurs
            session.abort_transaction()
            raise DatabaseTransactionAbort(reason=str(e)) from e

        try:
            session.commit_transaction()
        except InvalidOperation as e:
            raise DatabaseTransactionAbort(reason=str(e)) from e

    etag = create_etag(new_annotation.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS))

    response.headers["Allow"] = ANNOTATION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    response.headers["Etag"] = etag
    response.headers["Location"] = ANNOTATION_ID
    response.status_code = status.HTTP_201_CREATED
    return new_annotation


retrieve_annotation_options: Final[dict[str, Any]] = {
    "responses": {status.HTTP_404_NOT_FOUND: {"model": models.ErrorMessage}},
    "response_class": JSONLDResponse,
    "response_description": "The requested Annotation.",
    "response_model": models.Annotation,
    "response_model_exclude_none": True,
    "summary": "Retrieve a Web Annotation.",
    "tags": ["annotations"],
}


@router.get(
    "/{collection_slug}/{annotation_slug}/",
    **retrieve_annotation_options,
)
@router.get(
    "/{collection_slug}/{annotation_slug}",
    **retrieve_annotation_options,
    include_in_schema=False,
)
def retrieve_annotation(
    collection_slug: str, annotation_slug: str, response: JSONLDResponse
) -> models.Annotation:
    """Retrieve an Annotation (`annotation_slug`) from a
    Collection (`annotation_slug`).
    """

    if collection_slug in RESERVED_COLLECTION_NAMES:
        raise CollectionNameReserved()

    COLLECTION_ID: Final[str] = f"{BASE_URL}/{collection_slug}/"
    collection_document = COLLECTIONS.find_one({"id": COLLECTION_ID}, {"_id": False})
    if not collection_document:
        raise CollectionNotFound(collection_slug, COLLECTION_ID)

    ANNOTATION_ID: Final[str] = COLLECTION_ID + annotation_slug
    annotation_document = ANNOTATIONS.find_one({"id": ANNOTATION_ID}, {"_id": False})
    if not annotation_document:
        raise AnnotationNotFound(annotation_slug, ANNOTATION_ID)

    annotation = models.Annotation(**annotation_document)
    etag = create_etag(annotation.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS))

    response.headers["Allow"] = ANNOTATION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    response.headers["Etag"] = etag
    response.headers["Link"] = '<http://www.w3.org/ns/ldp#Resource>; rel="type"'
    response.headers["Vary"] = "Accept"

    return annotation


replace_annotation_options: Final[dict[str, Any]] = {
    "responses": {
        status.HTTP_404_NOT_FOUND: {"model": models.ErrorMessage},
        status.HTTP_412_PRECONDITION_FAILED: {"model": models.ErrorMessage},
        status.HTTP_500_INTERNAL_SERVER_ERROR: {"model": models.ErrorMessage},
    },
    "response_class": JSONLDResponse,
    "response_description": "The up-to-date Annotation.",
    "response_model": models.Annotation,
    "response_model_exclude_none": True,
    "summary": "Replace a Web Annotation.",
    "tags": ["annotations"],
}


@router.put(
    "/{collection_slug}/{annotation_slug}/",
    **replace_annotation_options,
)
@router.put(
    "/{collection_slug}/{annotation_slug}",
    **replace_annotation_options,
    include_in_schema=False,
)
def replace_annotation(
    collection_slug: str,
    annotation_slug: str,
    annotation: models.InputAnnotation,
    response: JSONLDResponse,  # ???
    if_match: Annotated[str | None, Header()] = None,
) -> models.Annotation:
    """
    Replace an Annotation (`annotation_slug`) in a Collection (`collection_slug`).

    **ATTENTION**: Calling this method is not idempotent because the `created` and
    `modified` fields are always newly created.
    """

    if collection_slug in RESERVED_COLLECTION_NAMES:
        raise CollectionNameReserved()

    COLLECTION_ID: Final[str] = f"{BASE_URL}/{collection_slug}/"
    collection_document = COLLECTIONS.find_one({"id": COLLECTION_ID}, {"_id": False})
    if not collection_document:
        raise CollectionNotFound(collection_slug, COLLECTION_ID)

    ANNOTATION_ID: Final[str] = COLLECTION_ID + annotation_slug
    old_annotation_document = ANNOTATIONS.find_one(
        {"id": ANNOTATION_ID}, {"_id": False}
    )
    if not old_annotation_document:
        raise AnnotationNotFound(annotation_slug, ANNOTATION_ID)

    old_annotation = models.Annotation(**old_annotation_document)
    etag: Final[str] = create_etag(
        old_annotation.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS)
    )

    if if_match != etag:
        raise AnnotationMismatch(annotation_slug, ANNOTATION_ID)

    timestamp: str = isotimestamp()

    new_annotation_document = annotation.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS)
    # Is the following logic doing the right thing???
    if (
        "id" in new_annotation_document
        and new_annotation_document["id"] != old_annotation_document["id"]
    ):
        new_annotation_document["via"] = new_annotation_document["id"]
        new_annotation_document["id"] = ANNOTATION_ID

    new_annotation_document["created"] = timestamp
    new_annotation_document["modified"] = timestamp
    new_annotation = models.Annotation(**new_annotation_document)

    with mongo_client.start_session() as session:
        try:
            session.start_transaction()
        except InvalidOperation as e:
            raise DatabaseTransactionAbort(reason=str(e)) from e

        try:
            # replace annotation
            output_annotation_document = ANNOTATIONS.find_one_and_replace(
                filter={"id": ANNOTATION_ID},
                replacement=new_annotation.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS),
                projection={"_id": False},
                return_document=ReturnDocument.AFTER,
            )

            # update "modified" in the collection document
            COLLECTIONS.update_one(
                {"id": COLLECTION_ID},
                {"$set": {"modified": timestamp}},
                session=session,
            )
        except Exception as e:
            # Abort transaction if any error occurs
            session.abort_transaction()
            raise DatabaseTransactionAbort(reason=str(e)) from e

        try:
            session.commit_transaction()
        except InvalidOperation as e:
            raise DatabaseTransactionAbort(reason=str(e)) from e

    output_annotation = models.Annotation(**output_annotation_document)
    new_etag = create_etag(output_annotation.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS))

    response.headers["Allow"] = ANNOTATION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    response.headers["Etag"] = new_etag
    response.headers["Location"] = ANNOTATION_ID

    return output_annotation


delete_annotation_options: Final[dict[str, Any]] = {
    "responses": {
        status.HTTP_404_NOT_FOUND: {"model": models.ErrorMessage},
        status.HTTP_412_PRECONDITION_FAILED: {"model": models.ErrorMessage},
    },
    **EMPTY_RESPONSE_OPTIONS,
    "summary": "Delete a Web Annotation.",
    "tags": ["annotations"],
}


@router.delete(
    "/{collection_slug}/{annotation_slug}/",
    **delete_annotation_options,
)
@router.delete(
    "/{collection_slug}/{annotation_slug}",
    **delete_annotation_options,
    include_in_schema=False,
)
def delete_annotation(
    collection_slug: str,
    annotation_slug: str,
    if_match: Annotated[str | None, Header()] = None,
) -> None:
    """Delete an Annotation (`annotation_slug`) in a Collection (`collection_slug`)."""

    if collection_slug in RESERVED_COLLECTION_NAMES:
        raise CollectionNameReserved()

    COLLECTION_ID: Final[str] = f"{BASE_URL}/{collection_slug}/"
    collection_document = COLLECTIONS.find_one({"id": COLLECTION_ID}, {"_id": False})
    if not collection_document:
        raise CollectionNotFound(collection_slug, COLLECTION_ID)

    ANNOTATION_ID: Final[str] = COLLECTION_ID + annotation_slug
    annotation_document = ANNOTATIONS.find_one({"id": ANNOTATION_ID}, {"_id": False})
    if not annotation_document:
        raise AnnotationNotFound(annotation_slug, ANNOTATION_ID)

    annotation = models.Annotation(**annotation_document)
    etag = create_etag(annotation.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS))

    if if_match != etag:
        raise AnnotationMismatch(annotation_slug, ANNOTATION_ID)

    ANNOTATIONS.delete_one({"id": ANNOTATION_ID})


empty_annotation_response_options: Final[dict[str, Any]] = {
    "responses": {
        status.HTTP_204_NO_CONTENT: {},
        status.HTTP_404_NOT_FOUND: {"model": models.ErrorMessage},
    },
    **EMPTY_RESPONSE_OPTIONS,
    "tags": ["annotations"],
}


@router.options(
    "/{collection_slug}/{annotation_slug}/",
    **empty_annotation_response_options,
)
@router.options(
    "/{collection_slug}/{annotation_slug}",
    **empty_annotation_response_options,
    include_in_schema=False,
)
# pylint: disable=missing-function-docstring,unused-argument
def options_annotation(
    collection_slug: str, annotation_slug: str, response: Response
) -> None:
    if collection_slug in RESERVED_COLLECTION_NAMES:
        raise CollectionNameReserved()
    COLLECTION_ID: Final[str] = f"{BASE_URL}/{collection_slug}/"
    collection_document = COLLECTIONS.find_one({"id": COLLECTION_ID}, {"_id": False})
    if not collection_document:
        raise CollectionNotFound(collection_slug, COLLECTION_ID)

    ANNOTATION_ID: Final[str] = COLLECTION_ID + annotation_slug
    annotation_document = ANNOTATIONS.find_one({"id": ANNOTATION_ID}, {"_id": False})
    if not annotation_document:
        raise AnnotationNotFound(annotation_slug, ANNOTATION_ID)
    response.headers["Allow"] = ANNOTATION_ALLOW


@router.head(
    "/{collection_slug}/{annotation_slug}/",
    **empty_annotation_response_options,
)
@router.head(
    "/{collection_slug}/{annotation_slug}",
    **empty_annotation_response_options,
    include_in_schema=False,
)
# pylint: disable=missing-function-docstring
def head_annotation(
    collection_slug: str, annotation_slug: str, response: Response
) -> None:
    if collection_slug in RESERVED_COLLECTION_NAMES:
        raise CollectionNameReserved()

    COLLECTION_ID: Final[str] = f"{BASE_URL}/{collection_slug}/"
    collection_document = COLLECTIONS.find_one({"id": COLLECTION_ID}, {"_id": False})
    if not collection_document:
        raise CollectionNotFound(collection_slug, COLLECTION_ID)

    ANNOTATION_ID: Final[str] = COLLECTION_ID + annotation_slug
    annotation_document = ANNOTATIONS.find_one({"id": ANNOTATION_ID}, {"_id": False})
    if not annotation_document:
        raise AnnotationNotFound(annotation_slug, ANNOTATION_ID)

    annotation = models.Annotation(**annotation_document)
    etag = create_etag(annotation.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS))

    response.headers["Allow"] = ANNOTATION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    response.headers["Etag"] = etag
    response.headers["Link"] = '<http://www.w3.org/ns/ldp#Resource>; rel="type"'
    response.headers["Vary"] = "Accept"
    response.status_code = status.HTTP_204_NO_CONTENT


def update_or_create_page(
    annotation_id: str,
    collection_document: dict[str, Any],
    session: ClientSession,
) -> str:
    """Update or create an Annotation Page.

    Args:
        annotation_id (str): ID of the Annotation to be added to the page
        collection_document (dict[str, Any]): Annotation Collection the page belongs to

    Raises:
        PageNotFound: The page an annotation is added to cannot be found

    Returns:
        str: ID of the "last" page in the collection
    """
    collection_id: Final[str] = collection_document["id"]
    # no pages, create a new first and, currently, last page
    if "first" not in collection_document.keys():
        first_page_id = collection_id + "?page=0"
        first_page = models.Page(
            id=HttpUrl(first_page_id),
            items=[HttpUrl(annotation_id)],
            partOf=HttpUrl(collection_id),
            startIndex=0,
        )
        PAGES.insert_one(
            first_page.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS), session=session
        )
        # update collection document's "first" field
        COLLECTIONS.update_one(
            {"id": collection_id},
            {"$set": {"first": first_page_id}},
            session=session,
        )
        return first_page_id
    # if there is a "first" page, there must be a "last" one
    last_page_id: str = collection_document["last"]
    last_page_rest_url, last_page_number = last_page_id.rsplit("=", 1)
    page_document = PAGES.find_one(
        {"id": last_page_id},
        {"_id": False},
        session=session,
    )
    if not page_document:
        raise PageNotFound(int(last_page_number), last_page_id)
    # think about whether this could be a list of Annotations in any case
    items: list[str] = page_document["items"]
    # page is full, create a new one
    if len(items) > DEFAULT_PAGE_SIZE - 1:
        next_page_number = int(last_page_number) + 1
        next_page_id = f"{last_page_rest_url}={str(next_page_number)}"
        next_page = models.Page(
            id=HttpUrl(next_page_id),
            items=[HttpUrl(annotation_id)],
            partOf=HttpUrl(collection_id),
            prev=HttpUrl(last_page_id),
            startIndex=next_page_number * DEFAULT_PAGE_SIZE,
        )
        PAGES.insert_one(
            next_page.model_dump(**DEFAULT_MODEL_DUMP_OPTIONS), session=session
        )
        # set "next" of the previous page
        PAGES.update_one(
            {"id": last_page_id},
            {"$set": {"next": next_page_id}},
            session=session,
        )
        return next_page_id

    items.append(annotation_id)
    PAGES.update_one(
        {"id": last_page_id},
        {"$set": {"items": items}},
        session=session,
    )
    return last_page_id
