# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: CC0-1.0

"""WAPCAS Exceptions."""

from fastapi import HTTPException

from wapcas.constants import (
    ANNOTATION_ALLOW,
    COLLECTION_ALLOW,
    PAGE_ALLOW,
    RESERVED_CHARACTERS,
)


class PageNotFound(HTTPException):
    """Raises HTTP 404."""

    def __init__(self, page: int, page_id: str) -> None:
        super().__init__(
            status_code=404,
            detail=f"Page '{page}' ({page_id}) does not exist.",
            headers={"Allow": PAGE_ALLOW},
        )


class CollectionNotFound(HTTPException):
    """Raises HTTP 404."""

    def __init__(self, collection_slug: str, collection_id: str) -> None:
        super().__init__(
            status_code=404,
            detail=f"Collection '{collection_slug}' ({collection_id}) does not exist.",
            headers={"Allow": COLLECTION_ALLOW},
        )


class CollectionNotEmpty(HTTPException):
    """Raises HTTP 409."""

    def __init__(self, collection_slug: str, collection_id: str) -> None:
        super().__init__(
            status_code=409,
            detail=f"Collection '{collection_slug}' ({collection_id}) is not empty.",
            headers={"Allow": COLLECTION_ALLOW},
        )


class AnnotationNotFound(HTTPException):
    """Raises HTTP 404."""

    def __init__(self, annotation_slug: str, annotation_id: str) -> None:
        super().__init__(
            status_code=404,
            detail=f"Annotation '{annotation_slug}' ({annotation_id}) does not exist.",
            headers={"Allow": ANNOTATION_ALLOW},
        )


class CollectionMismatch(HTTPException):
    """Raises HTTP 412."""

    def __init__(self, collection_slug: str, collection_id: str) -> None:
        super().__init__(
            status_code=412,
            detail=f"Collection '{collection_slug}' ({collection_id}) has changed.",
            headers={"Allow": COLLECTION_ALLOW},
        )


class AnnotationMismatch(HTTPException):
    """Raises HTTP 412."""

    def __init__(self, annotation_slug: str, annotation_id: str) -> None:
        super().__init__(
            status_code=412,
            detail=f"Annotation '{annotation_slug}' ({annotation_id}) has changed.",
            headers={"Allow": ANNOTATION_ALLOW},
        )


class CollectionExists(HTTPException):
    """Raises HTTP 409."""

    def __init__(self, collection_slug: str, collection_id: str) -> None:
        super().__init__(
            status_code=409,
            detail=f"Collection '{collection_slug}' ({collection_id}) exists.",
            headers={"Allow": COLLECTION_ALLOW},
        )


class CollectionNameReserved(HTTPException):
    """Raises HTTP 405."""

    def __init__(self) -> None:
        super().__init__(
            status_code=405,
            detail="Collection identifier reserved.",
            headers={"Allow": ""},
        )


class ReservedCharacterUsedInIdentifier(HTTPException):
    """Raises HTTP 405."""

    def __init__(self, slug: str, allow_header: str) -> None:
        super().__init__(
            status_code=403,
            detail=f"The identifier '{slug}' contains a reserved character (one of "
            f"{RESERVED_CHARACTERS}).",
            headers={"Allow": allow_header},
        )


# too generic? cannot set specific headers...but this is not required by spec
class DatabaseTimeout(HTTPException):
    """Raises HTTP 503."""

    def __init__(self) -> None:
        super().__init__(status_code=503, detail="Database connection timeout.")


class DatabaseTransactionAbort(HTTPException):
    """Raises HTTP 500."""

    def __init__(self, reason: str) -> None:
        super().__init__(
            status_code=500, detail=f"Database transaction aborted. Reason: {reason}"
        )


class AnnotationExists(HTTPException):
    """Raises HTTP 409."""

    def __init__(self, annotation_slug: str, annotation_id: str) -> None:
        super().__init__(
            status_code=409,
            detail=f"Annotation '{annotation_slug}' ({annotation_id}) exists.",
            headers={"Allow": ANNOTATION_ALLOW},
        )
