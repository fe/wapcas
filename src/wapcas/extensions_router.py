# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: EUPL-1.2

"""API extensions router."""

from fastapi import APIRouter, HTTPException, status
from pymongo import timeout
from pymongo.errors import ConnectionFailure

from dbinit import mongo_client
from wapcas.utils import DEFAULT_DB_TIMEOUT

router = APIRouter()


@router.get(
    "/healthz",
    include_in_schema=False,
)
def get_health() -> int:
    """Pings the database. Used for health checks/kubernetes probes."""
    try:
        with timeout(DEFAULT_DB_TIMEOUT):
            # returns "1.0" but in json, numbers with a zero fractional part are
            # considered integers
            return int(mongo_client.admin.command("ping")["ok"])
    except ConnectionFailure as e:
        raise HTTPException(
            status.HTTP_503_SERVICE_UNAVAILABLE, detail="No connection to the database."
        ) from e
