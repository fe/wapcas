# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: EUPL-1.2

"""Pydantic models for data validation by FastAPI."""

import json
from typing import Annotated, Any, Literal, Union

from pydantic import (
    BaseModel,
    ConfigDict,
    Discriminator,
    Field,
    HttpUrl,
    NonNegativeInt,
    PositiveInt,
    SerializeAsAny,
    Tag,
    field_serializer,
    field_validator,
    model_validator,
)

from wapcas.constants import (
    OA_ANNOTATION_CONTEXT_FIELD_DEFAULT_VALUE as ANNOTATION_CONTEXT_FIELD_DEFAULT_VALUE,
    OA_ANNOTATION_TYPE_FIELD_DEFAULT_VALUE as ANNOTATION_TYPE_FIELD_DEFAULT_VALUE,
    OA_COLLECTION_CONTEXT_FIELD_DEFAULT_VALUE as COLLECTION_CONTEXT_FIELD_DEFAULT_VALUE,
    OA_COLLECTION_TYPE_FIELD_DEFAULT_VALUE as COLLECTION_TYPE_FIELD_DEFAULT_VALUE,
    OA_COLLECTION_TYPE_NAME as COLLECTION_TYPE_NAME,
    OA_CONTEXT_FIELD_ALIAS as CONTEXT_FIELD_ALIAS,
    OA_CONTEXT_FIELD_DEFAULT_VALUE as CONTEXT_FIELD_DEFAULT_VALUE,
    OA_CONTEXT_FIELD_NAME as CONTEXT_FIELD_NAME,
    OA_PAGE_CONTEXT_FIELD_DEFAULT_VALUE as PAGE_CONTEXT_FIELD_DEFAULT_VALUE,
    OA_PAGE_TYPE_FIELD_DEFAULT_VALUE as PAGE_TYPE_FIELD_DEFAULT_VALUE,
    OA_TYPE_FIELD_NAME as TYPE_FIELD_NAME,
)
from wapcas.serializers import serialize_single_context_or_list
from wapcas.validators import HttpUrlString, value_of_key_contains_item

DEFAULT_MODEL_DUMP_OPTIONS: dict[str, Any] = {
    "by_alias": True,
    "mode": "json",
    "exclude_none": True,
}

type Selector = Union[
    "FragmentSelector",
    "CssSelector",
    "XPathSelector",
    "TextQuoteSelector",
    "TextPositionSelector",
    "DataPositionSelector",
    "SvgSelector",
]
"""All Selectors that can be used recursively (not RangeSelector)."""

type State = Union["HttpRequestState", "TimeState"]


class BaseSelector(BaseModel):
    """The selector base model."""

    refinedBy: Union[Selector, "RangeSelector"] | None = None
    model_config = ConfigDict(extra="allow")


class FragmentSelector(BaseSelector):
    """A resource which describes the Segment through the use of the fragment component
    of an IRI.
    """

    type: str = "FragmentSelector"
    """The class of the Selector."""
    value: str
    """The contents of the fragment component of an IRI that describes the Segment."""
    conformsTo: str | None = None  # should be IRI
    """The relationship between the FragmentSelector and the specification that defines
    the syntax of the IRI fragment in the value property.
    """


class CssSelector(BaseSelector):
    """The type of the CSS Selector resource."""

    type: str = "XPathSelector"
    """The class of the Selector."""
    value: str
    """The CSS selection path to the Segment."""


class XPathSelector(BaseSelector):
    """The type of the XPath Selector resource."""

    type: str = "CssSelector"
    """The class of the Selector."""
    value: str
    """The xpath to the selected segment."""


class TextQuoteSelector(BaseSelector):
    """The class for a Selector that describes a textual segment by means of quoting it,
    plus passages before or after it.
    """

    type: str = "TextQuoteSelector"
    """The class of the Selector."""
    exact: str
    """A copy of the text which is being selected, after normalization."""
    prefix: str | None = None
    """A snippet of text that occurs immediately before the text which is being
    selected.
    """
    suffix: str | None = None
    """The snippet of text that occurs immediately after the text which is being
    selected.
    """


class TextPositionSelector(BaseSelector):
    """The class for a Selector which describes a range of text based on its start and
    end positions.
    """

    type: str = "TextPositionSelector"
    """The class of the Selector."""
    start: NonNegativeInt
    """The starting position of the segment of text. The first character in the full
    text is character position 0, and the character is included within the segment.
    """
    end: NonNegativeInt
    """The end position of the segment of text. The character is not included within
    the segment.
    """


class DataPositionSelector(BaseSelector):
    """The class for a Selector which describes a range of data based on its start and
    end positions within the byte stream.
    """

    type: str = "DataPositionSelector"
    """The class of the Selector."""
    start: NonNegativeInt
    """The starting position of the segment of data.

    The first byte is character position 0.
    """
    end: NonNegativeInt
    """The end position of the segment of data.

    The last character is not included within the segment.
    """


class SvgSelector(BaseSelector):
    """The class for a Selector which defines a shape for the selected area using the
    SVG standard.
    """

    type: str = "SvgSelector"
    """The class of the Selector."""
    value: str | None = None
    """The character sequence of the SVG content."""


class RangeSelector(BaseSelector):
    """The type of the Range Selector resource."""

    type: str = "RangeSelector"
    startSelector: Selector
    """The Selector which describes the inclusive starting point of the range."""
    endSelector: Selector
    """The Selector which describes the exclusive ending point of the range."""


class _BaseState(BaseModel):
    """Base model for all State classes. For internal use only."""

    refinedBy: (
        State | Selector | RangeSelector | list[State | Selector | RangeSelector] | None
    ) = None
    """The relationship between a broader State and either a more specific State or a
    Selector that SHOULD be applied to the results of the first.
    """
    model_config = ConfigDict(extra="allow")


class TimeState(_BaseState):
    """A Time State resource records the time at which the resource is appropriate for
    the Annotation, typically the time that the Annotation was created and/or a link to
    a persistent copy of the current version.
    """

    type: Literal["TimeState"] = "TimeState"
    """The class of the State."""
    sourceDate: str | None = None
    """The timestamp at which the Source resource SHOULD be interpreted for the
    Annotation.
    """
    sourceDateStart: str | None = None
    """The timestamp that begins the interval over which the Source resource SHOULD be
    interpreted for the Annotation.
    """
    sourceDateEnd: str | None = None
    """The timestamp that ends the interval over which the Source resource SHOULD be
    interpreted for the Annotation.
    """
    cached: Any | None = None
    """A link to a copy of the Source resource's representation, appropriate for the
    Annotation.
    """

    @model_validator(mode="after")
    def validate_source_date(self):
        """Validates the model against the following constraints:
        - If sourceDate is provided, then sourceDateStart and sourceDateEnd MUST NOT be
        provided.
        - If sourceDateStart is provided then sourceDateEnd MUST also be provided.
        - If sourceDateEnd is provided then sourceDateStart MUST also be provided.
        """
        if self.sourceDate and (self.sourceDateStart or self.sourceDateEnd):
            raise ValueError(
                "Cannot have both sourceDate and sourceDateStart or sourceDateEnd "
                "properties."
            )
        if (self.sourceDateStart and not self.sourceDate) or (
            not self.sourceDateStart and self.sourceDateEnd
        ):
            raise ValueError(
                "Must have both sourceDateStart and sourceDateEnd properties."
            )

        return self


class HttpRequestState(_BaseState):
    """The HttpRequestState resource maintains a copy of the headers to be replayed when
    obtaining the representation.
    """

    type: Literal["HttpRequestState"] = "HttpRequestState"
    """The class of the State."""
    value: str
    """The HTTP request headers to send as a single, complete string."""


class SpecificResource(BaseModel):
    """The class for Specific Resources."""

    id: SerializeAsAny[HttpUrlString] | None = None
    """The identity of the Specific Resource."""
    type: Literal["SpecificResource"] = "SpecificResource"
    """The class of the Specific Resource."""
    source: SerializeAsAny[HttpUrlString] | Any
    """The relationship between a Specific Resource and the resource that it is a more
    specific representation of.
    """
    purpose: str | list[str] | None = None
    """The reason for including the Web Resource in the Annotation."""
    selector: (
        Selector
        | RangeSelector
        | SerializeAsAny[HttpUrlString]
        | list[Selector | RangeSelector | SerializeAsAny[HttpUrlString]]
        | None
    ) = None
    """The relationship between a Specific Resource and a Selector."""
    state: State | list[State] | None = None
    """The relationship between the SpecificResource and the State."""
    model_config = ConfigDict(extra="allow")


class _ResourceBaseModel(BaseModel):
    """A BaseModel for ExternalWebResource and EmbeddedTextualBody. Internal use
    only.
    """

    format: str | list[str] | None = (
        None  # https://www.w3.org/TR/annotation-model/#bib-iana-media-types
    )
    """The format of the Web Resource's content."""
    language: str | list[str] | None = (
        None  # https://www.w3.org/TR/annotation-model/#bib-w3c-language-tags
    )
    """The language of the Web Resource's content."""
    processingLanguage: str | None = None  # same as above
    """The language to use for text processing algorithms such as line breaking,
    hyphenation, which font to use, and similar.
    """
    textDirection: Literal["ltr", "rtl", "auto"] | None = None
    """The overall base direction of the text in the resource."""
    model_config = ConfigDict(extra="allow")


class ExternalWebResource(_ResourceBaseModel):
    """The body and target model."""

    id: SerializeAsAny[
        HttpUrlString
    ]  # may have a fragment component as the most simple selector
    """The IRI that identifies the Body or Target resource."""
    type: str | list[str] | None = None
    """The type of the Body or Target resource."""


class EmbeddedTextualBody(_ResourceBaseModel):
    """The embedded textual body model."""

    value: str
    """The character sequence of the content of the Textual Body."""


def target_and_body_type_discriminator(value: Any) -> str | None:
    """Determine the object type during validation and serialization for the complex
    types of body and target.
    """
    if isinstance(value, (str, HttpUrl)):
        return "httpurl"
    if isinstance(value, dict):
        if "source" in value:
            return "SpecificResource"
        if "value" in value:
            return "EmbeddedTextualBody"
        if value.get("type", None) == "Choice":
            return "Choice"
        return "ExternalWebResource"
    if isinstance(value, BaseModel):
        if hasattr(value, "source"):
            return "SpecificResource"
        if hasattr(value, "value"):
            return "EmbeddedTextualBody"
        if getattr(value, "type", None) == "Choice":
            return "Choice"
        return "ExternalWebResource"
    return None


class Choice(BaseModel):
    """A construction that conveys to a consuming application that it SHOULD select one
    of the listed resources to display to the user, and not render all of them.
    """

    id: SerializeAsAny[HttpUrlString] | None = None
    type: Literal["Choice"] = "Choice"
    items: list[
        Annotated[
            Union[
                Annotated[ExternalWebResource, Tag("ExternalWebResource")],
                Annotated[EmbeddedTextualBody, Tag("EmbeddedTextualBody")],
                Annotated[SpecificResource, Tag("SpecificResource")],
                Annotated[HttpUrlString, Tag("httpurl")],
            ],
            Discriminator(target_and_body_type_discriminator),
        ]
    ]


class _BaseAnnotation(BaseModel):
    """The basic Annotation class. Internal use only."""

    type: str | list[str] = ANNOTATION_TYPE_FIELD_DEFAULT_VALUE
    """The type of the Annotation."""
    target: (
        Annotated[
            Union[
                Annotated[ExternalWebResource, Tag("ExternalWebResource")],
                Annotated[SpecificResource, Tag("SpecificResource")],
                Annotated[SerializeAsAny[HttpUrlString], Tag("httpurl")],
            ],
            Discriminator(target_and_body_type_discriminator),
        ]
        | list[
            Annotated[
                Union[
                    Annotated[ExternalWebResource, Tag("ExternalWebResource")],
                    Annotated[SpecificResource, Tag("SpecificResource")],
                    Annotated[SerializeAsAny[HttpUrlString], Tag("httpurl")],
                ],
                Discriminator(target_and_body_type_discriminator),
            ]
        ]
    )
    """The relationship between an Annotation and its Target."""
    body: (
        Annotated[
            Union[
                Annotated[ExternalWebResource, Tag("ExternalWebResource")],
                Annotated[SpecificResource, Tag("SpecificResource")],
                Annotated[Choice, Tag("Choice")],
                Annotated[EmbeddedTextualBody, Tag("EmbeddedTextualBody")],
                Annotated[SerializeAsAny[HttpUrlString], Tag("httpurl")],
            ],
            Discriminator(target_and_body_type_discriminator),
        ]
        | list[
            Annotated[
                Union[
                    Annotated[ExternalWebResource, Tag("ExternalWebResource")],
                    Annotated[SpecificResource, Tag("SpecificResource")],
                    Annotated[Choice, Tag("Choice")],
                    Annotated[EmbeddedTextualBody, Tag("EmbeddedTextualBody")],
                    Annotated[HttpUrlString, Tag("httpurl")],
                ],
                Discriminator(target_and_body_type_discriminator),
            ]
        ]
        | None
    ) = None
    """The relationship between an Annotation and its Body."""

    model_config = ConfigDict(
        extra="allow",
    )

    @field_validator(TYPE_FIELD_NAME)
    @classmethod
    def default_must_be_in_type(cls, value: str | list[str]):
        """Call the custom validator."""
        return value_of_key_contains_item(
            TYPE_FIELD_NAME, value, ANNOTATION_TYPE_FIELD_DEFAULT_VALUE
        )


class _BaseAnnotationWithContext(_BaseAnnotation):
    """The Basic Annotation class with a context field. Internal use only."""

    context: SerializeAsAny[HttpUrlString] | list[SerializeAsAny[HttpUrlString]] = (
        Field(
            alias=CONTEXT_FIELD_ALIAS,
            default=HttpUrl(CONTEXT_FIELD_DEFAULT_VALUE),
            title="JSON-LD Context(s)",
        )
    )
    """The context that determines the meaning of the JSON as an Annotation."""

    @field_serializer(CONTEXT_FIELD_NAME)
    def _serialize_single_context_or_list(
        self,
        context: SerializeAsAny[HttpUrlString] | list[SerializeAsAny[HttpUrlString]],
    ):
        """Call the context serializer."""
        return serialize_single_context_or_list(context)

    @field_validator(CONTEXT_FIELD_NAME)
    @classmethod
    def default_must_be_in_context(cls, context: HttpUrlString | list[HttpUrlString]):
        """Call the context validator."""
        return value_of_key_contains_item(
            CONTEXT_FIELD_NAME, context, ANNOTATION_CONTEXT_FIELD_DEFAULT_VALUE
        )


class InputAnnotation(_BaseAnnotationWithContext):
    """Annotation as it can be submitted."""

    id: HttpUrl | None = None
    """The identity of the Annotation."""
    bodyValue: str | None = None
    """The string value of the Body of the Annotation."""

    @model_validator(mode="after")
    def check_body_value(self):
        """Check if input has both `body` and `bodyValue` (forbidden)."""
        if self.body and self.bodyValue:
            raise ValueError("May not contain both body and bodyValue field.")
        return self

    model_config = ConfigDict(
        extra="allow",
        # Allow an aliased field to be populated by its name as given by the model
        # attribute.
        populate_by_name=True,
        # Example for `openapi.json`.
        json_schema_extra={
            "example": {
                CONTEXT_FIELD_ALIAS: CONTEXT_FIELD_DEFAULT_VALUE,
                "id": "http://example.org/anno1",
                TYPE_FIELD_NAME: ANNOTATION_TYPE_FIELD_DEFAULT_VALUE,
                "body": "http://example.org/post1",
                "target": "http://example.com/page1",
            }
        },
    )


class Annotation(_BaseAnnotationWithContext):
    """An Annotation as it is returned by the API."""

    id: SerializeAsAny[HttpUrlString]
    """The identity of the Annotation."""
    created: str
    """The time at which the resource was created."""
    modified: str
    """The time at which the resource was modified, after creation."""
    via: str | None = None
    """The relationship between an Annotation, Body or Target and the IRI of where that
    resource was obtained from by the system that is making it available.
    """

    @model_validator(mode="after")
    def check_body_value(self):
        """Check for `bodyValue` field, which must be transformed to a
        EmbeddedTextualBody
        object.
        """
        if hasattr(self, "bodyValue"):
            raise ValueError("May not contain bodyValue field.")
        return self

    model_config = ConfigDict(
        extra="allow",
        # Allow an aliased field to be populated by its name as given by the model
        # attribute.
        populate_by_name=True,
        # Example for `openapi.json`.
        json_schema_extra={
            "example": {
                CONTEXT_FIELD_ALIAS: CONTEXT_FIELD_DEFAULT_VALUE,
                "id": "http://example.org/anno1",
                TYPE_FIELD_NAME: ANNOTATION_TYPE_FIELD_DEFAULT_VALUE,
                "created": "2024-02-15T10:24:20.872125+00:00",
                "modified": "2024-02-15T10:24:20.872125+00:00",
                "body": "http://example.org/post1",
                "target": "http://example.com/page1",
            }
        },
    )


class EmbeddedAnnotation(_BaseAnnotation):
    """An Annotation object embedded in a Page."""

    id: SerializeAsAny[HttpUrlString]
    """The identity of the Annotation."""
    via: str | None = None
    created: str
    modified: str

    model_config = ConfigDict(
        extra="allow",
        populate_by_name=True,
    )


class EmbeddedPage(BaseModel):
    """An Annotation Collection’s Page embedded in a collection document."""

    id: SerializeAsAny[HttpUrlString]
    type: str | list[str] = PAGE_TYPE_FIELD_DEFAULT_VALUE
    items: Union[list[EmbeddedAnnotation], list[SerializeAsAny[HttpUrlString]]]
    next: SerializeAsAny[HttpUrlString] | None = (
        None  # mandatory except for the last page
    )
    prev: SerializeAsAny[HttpUrlString] | None = (
        None  # mandatory except for the first page
    )
    startIndex: NonNegativeInt = 0

    @field_validator(TYPE_FIELD_NAME)
    @classmethod
    def default_must_be_in_type(cls, value: str | list[str]):
        """Call the custom validator."""
        return value_of_key_contains_item(
            TYPE_FIELD_NAME, value, PAGE_TYPE_FIELD_DEFAULT_VALUE
        )


class EmbeddedCollection(BaseModel):
    """An Annotation Collection embedded in a page document."""

    id: SerializeAsAny[HttpUrlString]
    modified: str
    created: str
    # An embedded collection must have at least one Annotation...
    total: PositiveInt
    # ...and cannot embed another page
    first: SerializeAsAny[HttpUrlString]
    last: SerializeAsAny[HttpUrlString]


class Page(EmbeddedPage):
    """An Annotation Collection’s Page."""

    context: SerializeAsAny[HttpUrlString] | list[SerializeAsAny[HttpUrlString]] = (
        Field(alias=CONTEXT_FIELD_ALIAS, default=HttpUrl(CONTEXT_FIELD_DEFAULT_VALUE))
    )
    partOf: EmbeddedCollection | SerializeAsAny[HttpUrlString]

    @field_serializer(CONTEXT_FIELD_NAME)
    def _serialize_single_context_or_list(
        self,
        context: SerializeAsAny[HttpUrlString] | list[SerializeAsAny[HttpUrlString]],
    ):
        """Call the context serializer."""
        return serialize_single_context_or_list(context)

    @field_validator(CONTEXT_FIELD_NAME)
    @classmethod
    def default_must_be_in_context(cls, context: HttpUrlString | list[HttpUrlString]):
        """Call the context validator."""
        return value_of_key_contains_item(
            CONTEXT_FIELD_NAME, context, PAGE_CONTEXT_FIELD_DEFAULT_VALUE
        )


class Collection(BaseModel):
    """An Annotation Collection."""

    context: list[SerializeAsAny[HttpUrlString]] = Field(
        alias=CONTEXT_FIELD_ALIAS,
        default=[HttpUrlString(url) for url in COLLECTION_CONTEXT_FIELD_DEFAULT_VALUE],
    )
    id: SerializeAsAny[HttpUrlString]
    type: str | list[str] = COLLECTION_TYPE_FIELD_DEFAULT_VALUE
    modified: str
    created: str
    total: NonNegativeInt = 0
    first: SerializeAsAny[HttpUrlString] | EmbeddedPage | None = None
    last: SerializeAsAny[HttpUrlString] | None = None

    model_config = ConfigDict(
        # Allow an aliased field to be populated by its name as given by the model
        # attribute.
        populate_by_name=True,
        # Example for `openapi.json`.
        json_schema_extra={
            "example": {
                CONTEXT_FIELD_ALIAS: json.dumps(COLLECTION_CONTEXT_FIELD_DEFAULT_VALUE),
                "id": "http://example.org/collection1",
                TYPE_FIELD_NAME: json.dumps(COLLECTION_TYPE_FIELD_DEFAULT_VALUE),
                "label": "Steampunk Annotations",
                "creator": "http://example.com/publisher",
                "created": "2024-02-15T10:24:20.872125+00:00",
                "modified": "2024-02-15T10:24:20.872125+00:00",
                "total": 42023,
                "first": "http://example.org/page1",
                "last": "http://example.org/page42",
            }
        },
    )

    @field_serializer(CONTEXT_FIELD_NAME)
    def _serialize_single_context_or_list(
        self,
        context: SerializeAsAny[HttpUrlString] | list[SerializeAsAny[HttpUrlString]],
    ):
        """Call the context serializer."""
        return serialize_single_context_or_list(context)

    # why validate? cannot be modified
    @field_validator(CONTEXT_FIELD_NAME)
    @classmethod
    def default_must_be_in_context(cls, context: HttpUrlString | list[HttpUrlString]):
        """Call the context validator."""
        return value_of_key_contains_item(
            CONTEXT_FIELD_ALIAS, context, CONTEXT_FIELD_DEFAULT_VALUE
        )

    @field_validator(TYPE_FIELD_NAME)
    @classmethod
    def default_must_be_in_type(cls, value: str | list[str]):
        """Call the custom validator."""
        return value_of_key_contains_item(TYPE_FIELD_NAME, value, COLLECTION_TYPE_NAME)


class ErrorMessage(BaseModel):
    """An Error message."""

    detail: str

    model_config = ConfigDict(
        json_schema_extra={"example": {"detail": "Your request failed because..."}},
    )
