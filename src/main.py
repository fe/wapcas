# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
# SPDX-FileContributor: Stefan Hynek
#
# SPDX-License-Identifier: CC0-1.0

"""WAPCAS entrypoint."""

import contextlib

import sentry_sdk
from fastapi import FastAPI

import wapcas.logging
from wapcas import annotations_router, collections_router, extensions_router

# Initialize Sentry but never fail.
with contextlib.suppress(Exception):
    sentry_sdk.init()

wapcas.logging.init()

app = FastAPI()

# The extensions router must be included first or "/_" will otherwise be caught by the
# collections or annotations router. This is due to the design of the WAP.
app.include_router(extensions_router.router, prefix="/_")
app.include_router(collections_router.router)
app.include_router(annotations_router.router)
