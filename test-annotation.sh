#!/bin/bash
# SPDX-FileCopyrightText: 2023, 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

run() {
  # $1 Collection ID
  # $2 Number or Items to create
  printf " ⚙️\tSending dummy data to $1\n"
  TEMPFILE=$(mktemp)
  for i in $(seq 1 $2); do
    # create Annotation Body Value
    FORTUNE=$(fortune | sed -e 's/[^a-zA-Z0-9 ]/ /g' | tr -d '\n')
    # create Annotation Type
    TYPES=("Annotation" "foobar")
    TYPE=${TYPES[ $RANDOM % ${#TYPES[@]} ]}
    # annotation template
    printf '{
      "@context": "http://www.w3.org/ns/anno.jsonld",
      "type": "Annotation",
      "body": {
        "type": "TextualBody",
        "value": "0####"
      },
      "target": "http://www.example.com/1####"
      }' | \
        sed -e "s/0####/${FORTUNE}/g" -e "s/1####/$(pwgen 4 1)/g"  | \
        sed -e "s/Annotation/${TYPE}/g" | \
    curl --silent -X POST -H 'Content-Type: application/json' -d @- $1 -o ${TEMPFILE}-data -w "%{http_code}" > ${TEMPFILE}
  if ! grep "201" ${TEMPFILE} > /dev/null; then printf " 🚨\tCould not store annotation: $(cat ${TEMPFILE}-data | jq '.detail.[0].msg')\n"; fi
  done
  printf "\33[2K\r ✅\tCreated $2 annotation items\n"
  rm $TEMPFILE
}

printf " ⚙️\tCreating Collection"
COLLECTION=$(curl -X POST localhost:8000/ --silent | jq -r .id)
printf "\r ✅\tCollection created: ${COLLECTION}\n"

run ${COLLECTION} 100

exit 0
